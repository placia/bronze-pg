package com.skplanet.okx.pg.constant;

public class PGConstants {

	private PGConstants() {}
	
	public static final String PROFILE_LOCAL = "local";
	public static final String PROFILE_DEV = "dev";
	public static final String PROFILE_PROD = "prod";
	
	public static final String DEFAULT_TIMEZONE = "Asia/Seoul";
	
	public static final String PG_API_TOKEN = "pg:api:token";
    public static final String PG_API_TOKEN_DISABLED = "pg:api:token_disabled";
    public static final String PG_API_HEADER_TOKEN_NAME = "X_PG_API_TOKEN";

    public static final String REDIS_USER_VERIFIER_KEY_PREFIX = "pg:user:verifier:";
    public static final String REDIS_USER_ADD_LOCK_PREFIX = "pg:user:addlock:";
    

    public static final String PG_OCB_CRD_PREFIX = "KX";
    
    public static final String PG_OKX_STATUS_ON = "0";
    public static final String PG_OKX_STATUS_OFF = "1";
    
    public static final String PG_OCB_NO_CRD_ADDED = "2";
    public static final String PG_OCB_NO_CRD_COMBINED = "3";
    
    
}
