package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.OCBPointCancelReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointCancelResDto;
import com.skplanet.okx.pg.api.dto.TxResType;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK110;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK111;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public abstract class AbstractNxMileCancelChargeOCBPoint extends NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> {
    public AbstractNxMileCancelChargeOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public OCBPointCancelResDto execute(OCBPointCancelReqDto pointReqDto) {
        NxMileIFK110 request = getNxMileIFK110(pointReqDto);
        NxMileIFK111 response;

        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFK111.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .putAll(request.valuesForException())
                    .build());
        }

        OCBPointCancelResDto pointResDto;

        try {
            failHandleService.test(request, response);
            pointResDto = getOcbPointCancelChargeResForSuccess(response);
        } catch (NxMileException e) {
            log.error("OCB포인트 충전취소요청 실패:{}:({}:{})", request.getTrackingNo(), pointReqDto.getOrgApprovalNo(), response.getResultCode(), e);
            pointResDto = getOcbPointCancelChargeResForFailure(response);
        }

        return pointResDto;
    }

    private OCBPointCancelResDto getOcbPointCancelChargeResForFailure(NxMileIFK111 response) {
        OCBPointCancelResDto pointResDto = new OCBPointCancelResDto();
        pointResDto.setResultType(TxResType.FAIL);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg(response.getMessage1());
        pointResDto.setTrackingNo(response.getTrackingNo());
        return pointResDto;
    }

    private OCBPointCancelResDto getOcbPointCancelChargeResForSuccess(NxMileIFK111 response) {
        OCBPointCancelResDto pointResDto = new OCBPointCancelResDto();
        pointResDto.setResultType(TxResType.SUCCESS);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg("SUCCESS");
        pointResDto.setBalance(response.getUsablePoint1());
        pointResDto.setApprovalNo(response.getApproveNo());
        pointResDto.setApprovalDate(LocalDateTime.parse(response.getApproveDate()+response.getApproveTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        pointResDto.setTrackingNo(response.getTrackingNo());
        return pointResDto;
    }

    protected abstract NxMileIFK110 getNxMileIFK110(OCBPointCancelReqDto pointReqDto);
}
