package com.skplanet.okx.pg.api.dto.payment;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(description = "자금 거래 준비의 응답 파라미터")
@Data
public class PayPrepareRes {

	private Merchant merchant;

	@ApiModelProperty(value = "실제 결제시 전달할 결제 토큰", required = true)	
	private List<DappBalance> dappBalanceList;
}
