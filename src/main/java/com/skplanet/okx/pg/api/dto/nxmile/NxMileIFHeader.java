package com.skplanet.okx.pg.api.dto.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import com.skplanet.ocb.tcp.annotation.NxmileLenColumn;
import com.skplanet.ocb.tcp.domain.nxmile.NxmileEncodeBuilder;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

@NoArgsConstructor
@Data
@Accessors(chain = true)
public class NxMileIFHeader implements NxmileEncodeBuilder {
    protected static final String NXMILE_CORP_CD = "924S";

    @NxmileColumn(size = 4)
    @NxmileComment(value = "전문코드")
    private String msgGrpCd = "";
    @NxmileColumn(size = 4)
    @NxmileComment(value = "기관코드")
    private String corpCd = "924S";
    @NxmileColumn(size = 8)
    @NxmileComment(value = "전송일자")
    private String sendDate = "";
    @NxmileColumn(size = 6)
    @NxmileComment(value = "전송시간")
    private String sendTime = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "추적번호")
    private String trackingNo = "";
    @NxmileColumn(size = 2)
    @NxmileComment(value = "전문구분")
    private String msgType = "ON";
    @NxmileColumn(size = 4, value = "0")
    @NxmileComment(value = "본문길아")
    @NxmileLenColumn
    @Setter(AccessLevel.PROTECTED)
    private Long bodyLen = 0L;
    @NxmileColumn(size = 2)
    @NxmileComment(value = "응답코드 대분류")
    private String resCdLcls = "";
    @NxmileColumn(size = 2)
    @NxmileComment(value = "응답코드")
    private String resCdScls = "";
    @NxmileColumn(size = 8)
    @NxmileComment(value = "Filler")
    private String fillerHeader = "";

    public NxMileIFHeader(String msgGrpCd, String resCode) {
        LocalDateTime now = LocalDateTime.now();
        setMsgGrpCd(msgGrpCd)
                .setCorpCd(ofEmpty(corpCd, NXMILE_CORP_CD))
                .setSendDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setSendTime(now.format(DateTimeFormatter.ofPattern("HHmmss")))
                .setTrackingNo(ofEmpty(getTrackingNo(), String.valueOf(new Random().nextInt(1000000000))))
                .setMsgType("ON")
                .setResCdLcls(ofEmpty(resCdLcls, Optional.ofNullable(resCode).map(r -> r.substring(0, 2)).orElse("  ")))
                .setResCdScls(ofEmpty(resCdScls, Optional.ofNullable(resCode).map(r -> r.substring(2)).orElse("  ")));
    }

    public String getResultCode() {
        return resCdLcls+resCdScls;
    }

    public boolean isSuccess() {
        return NxMileResultCode.SUCCESS.value().equals(getResultCode());
    }

    public Map<String, Object> valuesForException() {
        return ImmutableMap.<String, Object> builder()
                .put("msgGrpCd", getMsgGrpCd())
                .put("trackingNo", getTrackingNo())
                .put("sendDate", getSendDate())
                .put("sendTime", getSendTime())
                .put("resultCode", getResultCode())
                .build();
    }
}