package com.skplanet.okx.pg.api.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.skplanet.okx.pg.api.dto.bif.CreateWalletReq;
import com.skplanet.okx.pg.api.dto.bif.CreateWalletRes;
import com.skplanet.okx.pg.api.dto.bif.PaymentInfo;
import com.skplanet.okx.pg.api.dto.bif.PaymentResult;
import com.skplanet.okx.pg.api.dto.payment.DappBalance;
import com.skplanet.okx.pg.api.dto.payment.Merchant;

import reactor.core.publisher.Mono;

@Service
public class BIFService {

	private final WebClient webClientBIF;
	
	private String apiUrlCreateWallet = "/bif/api/wallet/createWallet";
	
	
	public BIFService(@Value("${my.bif.url}") String bifBaseUrl, WebClient.Builder webClientBuilder) {
		this.webClientBIF = webClientBuilder.baseUrl(bifBaseUrl).build();
	}
	
	
	public CreateWalletRes createWallet(long userSn) {
		CreateWalletReq req = new CreateWalletReq();
		req.setUserSn(userSn);
		
		WebClient.RequestHeadersSpec<?> requestSpec = webClientBIF
				  .post()
				  .uri(apiUrlCreateWallet)
				  .body(BodyInserters.fromObject(req));
		
		
		Mono<CreateWalletRes> res = requestSpec.retrieve().bodyToMono(CreateWalletRes.class);
		
		return res.block();
	}
	
	public PaymentInfo getPaymentInfo(long userSn, long targetUserSn, long price) {
		PaymentInfo pi = new PaymentInfo();
		Merchant merchant = new Merchant();
		
		merchant.setName("매장1");
		merchant.setUserSn(targetUserSn);
		
		pi.setMerchant(merchant);
		pi.getBalances().add(new DappBalance("Dapp1", "asdf1234", 1000));
		pi.getBalances().add(new DappBalance("Dapp2", "asdf1235", 1500));
		
		return pi;
	}
	
	public PaymentResult executePayment(long userSn, long targetUserSn, long totalPrice, String clientTx, Map<String, Object> clientJson, Map<String, Integer> tokenAmountMap) {
		PaymentResult pr = new PaymentResult();
		pr.setTransactionAddress("asdfasdfasdf");
		
		return pr;
	}
	
}
