package com.skplanet.okx.pg.api.interceptor;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.skplanet.okx.pg.constant.PGConstants;
import com.skplanet.okx.pg.support.exception.ForbbidenException;


public class CSRFInterceptor extends HandlerInterceptorAdapter {
    
    @Override
    public boolean preHandle(HttpServletRequest request,
               HttpServletResponse response, Object handler) throws Exception {
        
        String tokenHeaderStr = request.getHeader(PGConstants.PG_API_HEADER_TOKEN_NAME);
        
        if (StringUtils.isEmpty(tokenHeaderStr)) {
            throw new ForbbidenException("token error:invalid token", new HashMap());
        }
        else {
            return true;
        }
    }

}