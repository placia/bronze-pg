package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFA960 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "전체 데이터 건수")
    private Long gridCnt0 = 1L;
    @NxmileColumn(size = 1)
    @NxmileComment(value = "멤버십 프로그램ID")
    private String membershipPgmId = "A";
    @NxmileColumn(size = 2)
    @NxmileComment(value = "멤버십프로그램구분")
    private String membershipSvcType = "A1";
    @NxmileColumn(size = 1)
    @NxmileComment(value = "요청구분(1: 등록 / 2: 해지 / 3: 변경)")
    private String requestType = "1";
    @NxmileColumn(size = 4)
    @NxmileComment(value = "통합서비스코드")
    private String intgUseSvcCd = "CD06";
    @NxmileColumn(size = 1)
    @NxmileComment(value = "대상유형(C: 카드 / M: 회원)")
    private String targetType = "M";
    @NxmileColumn(size = 16, paddingBefore = false)
    @NxmileComment(value = "카드번호")
    private String cardNo = "";
    @NxmileColumn(size = 1)
    @NxmileComment(value = "서비스상태(1: 정상 / 2: 중지)")
    private String svcStatus = "1";
    @NxmileColumn(size = 8)
    @NxmileComment(value = "유효시작일자")
    private String effectiveStartDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @NxmileColumn(size = 8)
    @NxmileComment(value = "유효종료일자")
    private String effectiveEndDate = "29991231";
    @NxmileColumn(size = 8)
    @NxmileComment(value = "신청일자")
    private String subscribeDate = "";
    @NxmileColumn(size = 50)
    @NxmileComment(value = "Filler")
    private String filler = "";
    @NxmileColumn(size = 1)
    @NxmileComment(value = "웹서비스호출구분(N: framework호철 / S: servlet호출 / W: WebT)")
    private String webSvcCallType = "S";

    public NxMileIFA960(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFA960 of() {
        return NxMileIFA960.of(null);
    }

    public static NxMileIFA960 of(String resCode) {
        return new NxMileIFA960("A960", resCode);
    }

    public NxMileIFA960 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId(ofEmpty(getMembershipPgmId(), "A"))
                .setMembershipSvcType(ofEmpty(getMembershipSvcType(), "A1"))
                .setIntgUseSvcCd(ofEmpty(getIntgUseSvcCd(), "CD06"))
                .setTargetType(ofEmpty(getTargetType(), "M"))
                .setWebSvcCallType(ofEmpty(getWebSvcCallType(), "S"))
                .build();
    }
}
