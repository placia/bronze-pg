package com.skplanet.okx.pg.api.dto.payment;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "NxMile에게 합산 사용 요청의 입력 파라미터")
@Data
public class PayReqToNxMile {
	
	@NotNull
	@ApiModelProperty(value = "userSn", required = true, example = "1234")
	private long userSn;
	
	@NotNull
	@ApiModelProperty(value = "합산사용으로 차감할 금액", required = true, example = "1000")
	private long amount;

	@NotNull
	@ApiModelProperty(value = "ocb에 기록할 okx의 결제 트랜잭션값(추적용)", required = true, example = "aaaa")
	private String payTx;
	
	@NotNull
	@ApiModelProperty(value = "client한테 받은 트랜잭션값", required = true, example = "bbbb")
	private String clientTx;
}
