package com.skplanet.okx.pg.api.dto.user.verifier;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "verifier (user access token) 요청 파라미터")
@Data
public class VerifierReq {

	@NotNull private String chId;
	@NotNull private String chKey;
	@NotNull private long userSn;
	
}
