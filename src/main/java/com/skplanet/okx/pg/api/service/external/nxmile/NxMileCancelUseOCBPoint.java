package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.OCBPointCancelReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointCancelResDto;
import com.skplanet.okx.pg.api.dto.TxResType;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK410;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK411;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class NxMileCancelUseOCBPoint extends AbstractNxMileCancelUseOCBPoint {
    public NxMileCancelUseOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    protected NxMileIFK410 getNxMileIFK410(OCBPointCancelReqDto pointReqDto) {
        return (NxMileIFK410) NxMileIFK410.of()
                .setWcc("0")
                .setMerchantNo(pointReqDto.getMctNo())
                .setMerchantBizNo(pointReqDto.getBizNo())
                .setTrxDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(pointReqDto.getTxDate()))
                .setTrxTime(DateTimeFormatter.ofPattern("HHmmss").format(pointReqDto.getTxDate()))
                .setTrackData(pointReqDto.getCrdNo())
                .setOrgTrxDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(pointReqDto.getOrgTxDate()))
                .setOrgTrxApproveNo(pointReqDto.getOrgApprovalNo())
                .setSlipCd("22")
                .setTrxTypeCd("42")
                .setComplexPayYn("N")
                .setOrgTrxAmt(pointReqDto.getOrgTxOcbPoint())
                .setCancelRequestType(1L)
                .setWebSvcCallType("N")
                .setTrackingNo(pointReqDto.getTrackingNo());
    }
}
