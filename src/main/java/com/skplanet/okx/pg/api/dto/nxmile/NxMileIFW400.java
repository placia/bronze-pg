package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.*;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFW400 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 DATA건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멥버쉽프로그램ID(A)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분(A1)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "포인트 구분(O: OCB 포인트)")
    private String pointType = "";
    @NxmileColumn(size = 3, paddingBefore = false)
    @NxmileComment(value = "포인트 종류FALSE")
    private String pointKind = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "포인트 응답 구분FALSE")
    private String pointResponseType = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "제휴사코드FALSE")
    private String partnerCode = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호FALSE")
    private String merchantNo = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "요청구분(코드 값 참조)")
    private String requestType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원구분(1:일반 2:법인 3:개인사업자 5:단체)")
    private String memberType = "";
    @NxmileColumn(size = 16, paddingBefore = false)
    @NxmileComment(value = "카드번호FALSE")
    private String cardNo = "";
    @NxmileColumn(size = 4, paddingBefore = false)
    @NxmileComment(value = "카드코드FALSE")
    private String cardCode = "";
    @NxmileColumn(size = 88, paddingBefore = false)
    @NxmileComment(value = "CI(회원구분이 ‘1‘(일반)인 경우 유입 가능)")
    private String ci = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "회원IDFALSE")
    private String memberId = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "회원구분번호(회원구분이 '2','3' 인 경우 유입 가능)")
    private String memberTypeNo = "";
    @NxmileColumn(size = 16, paddingBefore = false, trim = false)
    @NxmileComment(value = "비밀번호FALSE")
    private String password = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "적립가능여부(Y/N)")
    private String isAccumableType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "사용가능여부(Y/N)")
    private String isUsableType = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래금액총합계(총구매금액)")
    private Long trxAmtTotSum = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1((포인트 1은 하이브리드 가맹점인 경우, 하이브리드포인트 포함되어 출력))")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "발생포인트1FALSE")
    private Long occrPoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용 가능한 포인트)")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명2((포인트 2은 하이브리드 가맹점인 경우, 하이브리드포인트만 출력))")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "발생포인트2FALSE")
    private Long occrPoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트2(사용가능한 포인트)")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트2(현재포인트 + 가용포인트)")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 3, paddingBefore = false)
    @NxmileComment(value = "적용대상서비스FALSE")
    private String applyTargetSvc = "";
    @NxmileColumn(size = 117, paddingBefore = false)
    @NxmileComment(value = "filler FALSE")
    private String filler2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "웹서비스호출구분(내부용(N:프레임웍호출, S:servlet호출, W:WebT))")
    private String webSvcCallType = "";

    public NxMileIFW400(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFW400 of() {
        return NxMileIFW400.of(null);
    }

    public static NxMileIFW400 of(String resCode) {
        return new NxMileIFW400("W400", resCode);
    }

    public NxMileIFW400 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId(ofEmpty(getMembershipPgmId(), "A"))
                .setMembershipSvcType(ofEmpty(getMembershipSvcType(), "A1"))
                .setPointType("O")
                .setRequestType(ofEmpty(getRequestType(),"WN"))
                .setMemberType(ofEmpty(getMemberType(),"1"))
                .build();
    }
}
