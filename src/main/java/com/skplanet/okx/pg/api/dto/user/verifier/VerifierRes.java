package com.skplanet.okx.pg.api.dto.user.verifier;


import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "verifier (user access token) 응답 파라미터")
@Data
public class VerifierRes {

	private String verifier;
	
}
