package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD40 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "전체 Data건수('001')")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch)('0')")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "‘12345678       ‘('644401486      ')")
    private String merchantNo = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "(Space)")
    private Long merchantBizNo = 0L;
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "YYYYMMDD('20170215')")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "hhmmss('101010')")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "카드번호('1234567890123456                     ')")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "‘-‘제외(Space)")
    private String ssn = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "('훼미리마트 성북점                       ')")
    private String partnerMerchantName = "";
    @NxmileColumn(size = 25, paddingBefore = false)
    @NxmileComment(value = "('98022684                 ')")
    private String partnerMerchantNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "('521000004   ')")
    private String partnerApproveNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "YYYYMMDD, 항목설명 참조('20170215')")
    private String orgTrxDate = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "('000000001   ')")
    private String orgApproveNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "('521000001   ')")
    private String orgTrxPartnerApproveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "통합승인사용(Space)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "통합승인사용(Space)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "대외기관사용(Space)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "대외기관사용(Space)")
    private String message4 = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "11('11')")
    private String slipCd = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "40('40')")
    private String trxTypeCd = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "N(N)")
    private String complexPayYN = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "실사용 금액('0000030000')")
    private Long orgTrxAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "‘0’:적립/할인+현금취소, ‘1’:적립/할인Only취소('1')")
    private Long cancelRequestType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "1:거래취소,2:오류취소,3:기타(Space)")
    private String cashReceiptCancelRsn = "";

    public NxMileIFDD40(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD40 of() {
        return NxMileIFDD40.of(null);
    }

    public static NxMileIFDD40 of(String resCode) {
        return new NxMileIFDD40("DD40", resCode);
    }

    public NxMileIFDD40 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId("A")
                .setMembershipSvcType("A1")
                .build();
    }
}
