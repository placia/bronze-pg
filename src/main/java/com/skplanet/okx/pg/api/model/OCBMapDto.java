package com.skplanet.okx.pg.api.model;

import lombok.Data;

@Data
public class OCBMapDto {
	private long userSn;
	private String crdNo;
	private String ocbStatus;
}
