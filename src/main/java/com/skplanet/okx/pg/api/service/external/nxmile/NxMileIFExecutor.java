package com.skplanet.okx.pg.api.service.external.nxmile;

import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class NxMileIFExecutor<T, R>  {
    protected final NxMileFailHandleService failHandleService;
    protected final NxMileConn nxMileConn;

    public abstract R execute(T t);
}
