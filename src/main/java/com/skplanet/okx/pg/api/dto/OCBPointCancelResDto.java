package com.skplanet.okx.pg.api.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OCBPointCancelResDto {
    private TxResType resultType;
    private String resultCode;
    private String resultMsg;
    private String trackingNo;
    private String approvalNo;
    private LocalDateTime approvalDate;
    private long balance;
}
