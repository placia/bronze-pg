package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD41 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "전체 Data건수('001')")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch)('0')")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "‘12345678       ‘('644401486      ')")
    private String merchantNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "YYYYMMDD('20170215')")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "hhmmss('101010')")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "카드번호('1234567890123456                     ')")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "‘-‘제외(Space)")
    private String ssn = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "YYYYMMDD('20170215')")
    private String approveDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "hhmmss('100013')")
    private String approveTime = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "‘123456789‘ 숫자만 사용한다.('000000004   ')")
    private String approveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "통합승인사용(Space)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "통합승인사용(Space)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "대외기관사용(Space)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "대외기관사용(Space)")
    private String message4 = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "사용가능한 포인트('0000230000')")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "현재포인트 + 가용포인트('0000230000')")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "('0000000000')")
    private Long realSubstractPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "(Space)")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "사용가능한 포인트('0000000000')")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "현재포인트 + 가용포인트('0000000000')")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "('0000000000')")
    private Long realSubstractPoint2 = 0L;
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "→현금 취소처리 안된 이유 코드집 활용(Space)")
    private String cashCancelProcType = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "현금영수증 오류 관련 메시지(Space)")
    private String cashCancelErrorMessage = "";
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "현금거래금액(공급가액+부가세+봉사료)('000000000')")
    private Long cashTrxCancelAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "('000000000')")
    private Long splyAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "('000000000')")
    private Long vat = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "('000000000')")
    private Long serviceAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "소비자소득공제용(0:기본값)사업자지출증빙용(1)('0')")
    private Long usageType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "1:거래취소,2:오류취소,3:기타(Space)")
    private String cashReceiptCancelRsn = "";

    public NxMileIFDD41(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD41 of() {
        return NxMileIFDD41.of(null);
    }

    public static NxMileIFDD41 of(String resCode) {
        return new NxMileIFDD41("DD41", resCode);
    }

    public NxMileIFDD41 defaultValue() {
        return setGridCnt0(1L)
                .build();
    }
}
