package com.skplanet.okx.pg.api.controller;

import com.skplanet.ocb.tcp.annotation.TcpController;
import com.skplanet.ocb.tcp.annotation.TcpMapping;
import com.skplanet.okx.pg.api.dto.nxmile.*;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@TcpController
public class NxMileMockController {
    private static AtomicLong approvalNo = new AtomicLong(1);

    @TcpMapping("W400")
    public NxMileIFW400 w400(NxMileIFW400 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());

        request.setMsgGrpCd("W401");
        request.setResCdLcls("00");
        request.setResCdScls("00");
        request.setUsablePoint1(100000L);

        return request;
    }

    @TcpMapping("M480")
    public NxMileIFW400 m480(NxMileIFW400 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());

        request.setMsgGrpCd("W481");
        request.setResCdLcls("00");
        request.setResCdScls("00");

        return request;
    }

    @TcpMapping("A960")
    public NxMileIFW400 a960(NxMileIFW400 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());

        request.setMsgGrpCd("A961");
        request.setResCdLcls("00");
        request.setResCdScls("00");

        return request;
    }

    @TcpMapping("K400")
    public NxMileIFK401 k400(NxMileIFW400 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());

        LocalDateTime now = LocalDateTime.now();

        NxMileIFK401 k401 = new NxMileIFK401();
        k401.setMsgGrpCd("K401");
        k401.setResCdScls("00");
        k401.setResCdLcls("00");
        k401.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        k401.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        k401.setApproveNo(String.valueOf(approvalNo.getAndIncrement()));
        k401.setUsablePoint1(100000L);
        k401.setMessage3("1000");

        return k401;
    }

    @TcpMapping("K410")
    public NxMileIFK411 k410(NxMileIFW400 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());

        LocalDateTime now = LocalDateTime.now();

        NxMileIFK411 k411 = new NxMileIFK411();
        k411.setMsgGrpCd("K411");
        k411.setResCdLcls("00");
        k411.setResCdScls("00");
        k411.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        k411.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        k411.setApproveNo(String.valueOf(approvalNo.getAndIncrement()));
        k411.setUsablePoint1(100000L);

        return k411;
    }

    @TcpMapping("K100")
    public NxMileIFK101 k100(NxMileIFK100 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK101 k101 = new NxMileIFK101();
        k101.setMsgGrpCd("K101");
        k101.setResCdScls("00");
        k101.setResCdLcls("00");
        k101.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        k101.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        k101.setApproveNo(String.valueOf(approvalNo.getAndIncrement()));
        k101.setUsablePoint1(100000L);

        return k101;
    }

    @TcpMapping("K110")
    public NxMileIFK111 k110(NxMileIFK100 request) {
        log.info("{} recv: {}", request.getMsgGrpCd(), request.toString());
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK111 k111 = new NxMileIFK111();
        k111.setMsgGrpCd("K111");
        k111.setResCdLcls("00");
        k111.setResCdScls("00");
        k111.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        k111.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        k111.setApproveNo(String.valueOf(approvalNo.getAndIncrement()));
        k111.setUsablePoint1(100000L);

        return k111;
    }
}
