package com.skplanet.okx.pg.api.dto.bif;


import lombok.Data;

@Data
public class PaymentResult {
	private String transactionAddress;
	
}
