package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFM480;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class NxMileAddCard extends NxMileIFExecutor<NxMileAddCard.Request, Void> {
    public NxMileAddCard(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public Void execute(NxMileAddCard.Request addCardReq) {
        NxMileIFM480 request = NxMileIFM480.of()
                .setCi(addCardReq.getCi())
                .setCardNo(addCardReq.getCrdNo());

        NxMileIFM480 response = null;
        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFM480.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .put("ci", addCardReq.getCi())
                    .put("crdNo", addCardReq.getCrdNo())
                    .putAll(request.valuesForException())
                    .build());
        }

        failHandleService.test(request, response);

        return null;
    }

    @Getter
    @Builder
    public static class Request {
        private String ci;
        private String crdNo;
    }
}
