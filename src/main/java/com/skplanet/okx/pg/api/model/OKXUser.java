package com.skplanet.okx.pg.api.model;


import lombok.Data;

@Data
public class OKXUser {
	
	private long userSn;
	private String userKey;
	private String walletKey;
	private String userStatus; // 0-사용, 1-중지
}
