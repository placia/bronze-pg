package com.skplanet.okx.pg.api.mapper

import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.SelectKey
import org.springframework.transaction.annotation.Transactional

import com.skplanet.okx.pg.api.model.OCBMapDto


public interface ChannelMapper {

	@Select("""
		SELECT ch_status FROM OKX_CHANNEL WHERE ch_id = #{chId} and ch_key = #{chKey}
	""")
	String getChStatus(@Param("chId") String chId, @Param("chKey") String chKey);
	
	@Insert("""
		INSERT INTO OKX_CHANNEL (ch_id, ch_key, ch_status) VALUES (#{chId}, #{chKey}, #{chStatus})
	""")
	int insertChannel(@Param("chId") String chId, @Param("chKey") String chKey, @Param("chStatus") String chStatus);
	
}
