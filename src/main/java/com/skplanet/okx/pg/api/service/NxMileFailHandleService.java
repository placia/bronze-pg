package com.skplanet.okx.pg.api.service;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.nxmile.*;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import com.skplanet.okx.pg.support.exception.impl.NxMileNetCancelException;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class NxMileFailHandleService {
    private static Map<String, List<NxMileErrorPredicate>> errorPredicate;

    static {
        if (errorPredicate == null) {
            // @formatter:off
            errorPredicate = ImmutableMap.<String, List<NxMileErrorPredicate>> builder()
                    .put("W401",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> NxMileResultCode.INVALID_USER.isEquals(resp.getResultCode()),
                                        (req, resp) -> new NxMileException(NxMileResultCode.INVALID_USER.getMessage(), ImmutableMap.<String, Object> builder()
                                            .put("ci", ((NxMileIFW400) req).getCi())
                                            .putAll(resp.valuesForException())
                                            .build())
                                ),
                                NxMileErrorPredicate.of(
                                        (req, resp) -> "N".equals(((NxMileIFW400) resp).getIsAccumableType()),
                                        (req, resp) -> new NxMileException("탈회상태", ImmutableMap.<String, Object> builder()
                                            .put("ci", ((NxMileIFW400) req).getCi())
                                            .putAll(resp.valuesForException())
                                            .build())
                                ),
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess(),
                                        (req, resp) -> new NxMileException(String.format("%s [%s]", NxMileResultCode.DEFAULT.getMessage(), resp.getResultCode()), ImmutableMap.<String, Object> builder()
                                            .put("ci", ((NxMileIFW400) req).getCi())
                                            .putAll(resp.valuesForException())
                                            .build())
                                )
                            ))
                    .put("M481",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess(),
                                        (req, resp) -> new NxMileException(String.format("%s [%s]", NxMileResultCode.DEFAULT.getMessage(), resp.getResultCode()), ImmutableMap.<String, Object> builder()
                                            .put("ci", ((NxMileIFM480) req).getCi())
                                            .put("crdNo", ((NxMileIFM480) req).getCardNo())
                                            .putAll(resp.valuesForException())
                                            .build())
                                )
                            ))
                    .put("A961",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess(),
                                        (req, resp) -> new NxMileException(String.format("%s [%s]", NxMileResultCode.DEFAULT.getMessage(), resp.getResultCode()), ImmutableMap.<String, Object> builder()
                                            .put("crdNo", ((NxMileIFA960) req).getCardNo())
                                            .putAll(resp.valuesForException())
                                            .build())
                                )
                            ))
                    .put("K401",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> NxMileResultCode.SYSTEM_ERROR1.isEquals(resp.getResultCode()) ||
                                                       NxMileResultCode.SYSTEM_ERROR2.isEquals(resp.getResultCode()) ||
                                                       NxMileResultCode.TIME_OVER.isEquals(resp.getResultCode()),
                                        (req, resp) -> new NxMileNetCancelException(
                                            String.format("%s [%s]", NxMileResultCode.findErrorCode(resp.getResultCode()).getMessage(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                ),
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess(),
                                        (req, resp) -> new NxMileException(
                                            String.format("%s [%s]", ((NxMileIFK401) resp).getMessage1(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                )
                            ))
                    .put("K411",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess() && !NxMileResultCode.NO_APPR_NO.isEquals(resp.getResultCode()),
                                        (req, resp) -> new NxMileException(
                                            String.format("%s [%s]", ((NxMileIFK411) resp).getMessage1(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                )
                            ))
                    .put("K101",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> NxMileResultCode.SYSTEM_ERROR1.isEquals(resp.getResultCode()) ||
                                                       NxMileResultCode.SYSTEM_ERROR2.isEquals(resp.getResultCode()) ||
                                                       NxMileResultCode.TIME_OVER.isEquals(resp.getResultCode()),
                                        (req, resp) -> new NxMileNetCancelException(
                                            String.format("%s [%s]", NxMileResultCode.findErrorCode(resp.getResultCode()).getMessage(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                ),
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess(),
                                        (req, resp) -> new NxMileException(
                                            String.format("%s [%s]", ((NxMileIFK101) resp).getMessage1(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                )
                            ))
                    .put("K111",
                            Arrays.asList(
                                NxMileErrorPredicate.of(
                                        (req, resp) -> !resp.isSuccess() && !NxMileResultCode.NO_APPR_NO.isEquals(resp.getResultCode()),
                                        (req, resp) -> new NxMileException(
                                            String.format("%s [%s]", ((NxMileIFK411) resp).getMessage1(), resp.getResultCode()),
                                            ImmutableMap.<String, Object> builder().putAll(resp.valuesForException()).build()
                                        )
                                )
                            ))
                    .build();
            // @formatter:on
        }
    }

    public Stream<NxMileErrorPredicate> getPredicate(String msgGrpCd) {
        return Optional.ofNullable(errorPredicate.get(msgGrpCd))
                .map(x -> x.stream())
                .orElse(Stream.<NxMileErrorPredicate> empty())
                ;
    }

    /**
     * 에러인경우 NxMileException 발생, 정상인경우 아무것도 안함.
     * @param req, resp
     * @throws NxMileException
     */
    public void test(final NxMileIFHeader req, final NxMileIFHeader resp) throws NxMileException {
        Objects.requireNonNull(resp, "NXMILE 응답객체가 존재하지 않습니다.");

        getPredicate(resp.getMsgGrpCd()).forEach(p -> p.test(req, resp));
    }
}
