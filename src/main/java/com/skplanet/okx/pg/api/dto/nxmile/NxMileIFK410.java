package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFK410 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램ID(A:OKCashbag)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분(코드값 참조)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분(단말기구분코드 참조)")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호(CAT ID)")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678       ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가맹점사업자번호FALSE")
    private Long merchantBizNo = 0L;
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "회원구분번호(법인번호 또는 개인사업자번호)")
    private String memberTypeNo = "";
    @NxmileColumn(size = 88, paddingBefore = false)
    @NxmileComment(value = "CI(CI(신규))")
    private String ci = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler2(여유분(신규))")
    private String filler1 = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "회원ID(NXM회원ID(신규))")
    private String memberId = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "제휴사 회원IDFALSE")
    private String partnerMemberId = "";
    @NxmileColumn(size = 25, paddingBefore = false)
    @NxmileComment(value = "제휴사 회원IP(Client IP)")
    private String partnerMemberIp = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "제휴사 승인번호FALSE")
    private String partnerApproveNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "원거래일자(YYYYMMDD, 항목설명 참조)")
    private String orgTrxDate = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "원거래승인번호FALSE")
    private String orgTrxApproveNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "원거래제휴사승인번호FALSE")
    private String orgTrxPartnerApproveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "전표코드(코드값 참조)")
    private String slipCd = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "거래구분코드(코드값 참조)")
    private String trxTypeCd = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "복합결제여부(Y/N)")
    private String complexPayYn = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "원거래금액(복합겔제시 총 거래금액)")
    private Long orgTrxAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "취소 요청구분(‘0’:적립/할인+현금취소, ‘1’:적립/할인Only취소)")
    private Long cancelRequestType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "웹서비스호출구분(내부용(N:프레임웍호출, S:servlet호출, W:WebT))")
    private String webSvcCallType = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler(여유분(신규))")
    private String filler2 = "";

    public NxMileIFK410(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFK410 of() {
        return NxMileIFK410.of(null);
    }

    public static NxMileIFK410 of(String resCode) {
        return new NxMileIFK410("K410", resCode);
    }

    public NxMileIFK410 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId("A")
                .setMembershipSvcType("A1")
                .build();
    }
}
