package com.skplanet.okx.pg.api.dto.payment;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(description = "자금 거래 준비의 입력 파라미터")
@Data
public class PayPrepareReq {
	
	@NotNull
	@ApiModelProperty(value = "api사용을 위한 토큰", required = true, example = "asdf...")
	private String verifier;

	@NotNull
	@ApiModelProperty(value = "전송받을 사람의 userSn", required = true, example = "1234")
	private long targetUserSn;

	@NotNull
	@ApiModelProperty(value = "전송요청", required = true, example = "1000")
	private long price;
}
