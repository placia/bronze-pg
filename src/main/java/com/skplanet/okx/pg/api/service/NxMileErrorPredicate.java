package com.skplanet.okx.pg.api.service;

import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFHeader;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.RequiredArgsConstructor;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;

@RequiredArgsConstructor(staticName = "of")
public class NxMileErrorPredicate {
    private final BiPredicate<NxMileIFHeader, NxMileIFHeader> predicate;
    private final BiFunction<NxMileIFHeader, NxMileIFHeader, NxMileException> exceptionFunction;

    public void test(NxMileIFHeader req, NxMileIFHeader resp) throws NxMileException {
        if (predicate.test(req, resp))
            throw exceptionFunction.apply(req, resp);
    }
}
