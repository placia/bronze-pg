package com.skplanet.okx.pg.api.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OCBPointUseReqDto {
    private String trackingNo;
    private String mctNo;
    private long bizNo;
    private LocalDateTime txDate;
    private String crdNo;
    private String ci;
    private long totalAmt;
    private long txOcbPoint;
}
