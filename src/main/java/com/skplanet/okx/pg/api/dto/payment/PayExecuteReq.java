package com.skplanet.okx.pg.api.dto.payment;

import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(description = "자금 거래 실행의 입력 파라미터")
@Data
public class PayExecuteReq {

	@NotNull
	@ApiModelProperty(value = "api사용을 위한 토큰", required = true, example = "asdf...")
	private String verifier;

	@NotNull
	@ApiModelProperty(value = "전송받을 사람의 userSn", required = true, example = "1234")
	private long targetUserSn;

	@NotNull
	@ApiModelProperty(value = "전송요청", required = true, example = "1000")
	private long price;

	@Size(min=2, max=20)
	@NotNull
	@ApiModelProperty(value = "업체측 결제번호", required = true, example = "asdf..")
	private String clientTx;

	@NotNull
	@ApiModelProperty(value = "업체측에서 결제시 저장하고 싶은 값들", required = true, example = "{\"상품명\":\"과자1봉지\"}")
	private Map<String, Object> clientJson;

	@NotNull
	@ApiModelProperty(value = "결제시 이용할 각 토큰별 금액들(전체 결제 금액중 어떤 토큰을 이용할지에 대한 내용), key=dApp addr, value=사용할금액", required = true)
	private Map<String, Integer> tokenAmountMap;
	
}
