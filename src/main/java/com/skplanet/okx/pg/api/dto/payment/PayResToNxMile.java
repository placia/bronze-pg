package com.skplanet.okx.pg.api.dto.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "NxMile에게 합산 사용 요청의 응답 파라미터")
@Data
public class PayResToNxMile {
	
	@ApiModelProperty(value = "ocb로부터 받은 합산사용 결과 추적값", required = true, example = "aaaa")
	private String ocbTx;

}
