package com.skplanet.okx.pg.api.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.okx.pg.api.dto.payment.OcbPointReq;
import com.skplanet.okx.pg.api.dto.payment.PayReqToNxMile;
import com.skplanet.okx.pg.api.dto.payment.PayResToNxMile;
import com.skplanet.okx.pg.api.dto.user.add.OCBUserAddResDto;
import com.skplanet.okx.pg.api.dto.user.add.OKXUserAddReqDto;
import com.skplanet.okx.pg.api.dto.user.verifier.Verifier;
import com.skplanet.okx.pg.api.mapper.OCBMapper;
import com.skplanet.okx.pg.api.mapper.OKXUserMapper;
import com.skplanet.okx.pg.api.model.OCBMapDto;
import com.skplanet.okx.pg.api.model.OKXUser;
import com.skplanet.okx.pg.constant.PGConstants;
import com.skplanet.okx.pg.support.exception.SystemException;
import com.skplanet.okx.pg.support.exception.impl.GeneralBusinessException;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OCBService {

	@Autowired
	private NxMileIFService nxMile;

	@Autowired
	private OKXUserService okxUserService;

	@Autowired
	private OCBMapper ocbMapper;
	
	@Autowired
	private OKXUserMapper userMapper;

	@Autowired
	private ChannelService channelService;

	public OCBUserAddResDto addUser(OKXUserAddReqDto uard) {

		OCBUserAddResDto res = new OCBUserAddResDto();

		///////////////////////////////////////
		// OCB 특화 영역
		///////////////////////////////////////

		// ci 유효성을 nxMile 에서 확인
		// 가입유무 상관없이 무조건 가입시킬꺼니, 상태를 확인할 필요가 없다.
		// 혹시 가입불가(탈퇴중)인 오류코드를 알아낼 수가 있다면 좋겠는데.
		// nxMile.validateOcbUser(uard.getUserKey());

		/////////////////////////////////////////
		// OKX 공통 영역
		/////////////////////////////////////////

		// oxkUser/Wallet 추가 (공통 영역)
		OKXUser okxUser = okxUserService.addUser(uard);

		res.setUserSn(okxUser.getUserSn());
		res.setStatus(PGConstants.PG_OKX_STATUS_ON);

		/////////////////////////////////////////
		// OCB 특화 영역
		/////////////////////////////////////////

		// redis를 이용해 트렌잭션 걸기 userSn으로 제약
		if (!okxUserService.getUserAddLock(okxUser.getUserSn())) {
			throw new GeneralBusinessException("기존 가입 요청 처리중 입니다. 잠시후 재시도 바랍니다.");
		}

		OCBMapDto ocbMap = null;

		try {
			// ocb_map 이 있는지 확인
			ocbMap = ocbMapper.findOcbMapByUserSn(okxUser.getUserSn());

			// ocb_map이 아예 없다! 혹은 카드번호가 없다
			if (ocbMap == null) {
				// ocbMap object 생성
				ocbMap = new OCBMapDto();
				ocbMap.setOcbStatus(PGConstants.PG_OCB_NO_CRD_ADDED);
			}
			// 이미 잘 등록되어 있으면 그냥 skip
			else if (PGConstants.PG_OKX_STATUS_ON.equals(ocbMap.getOcbStatus())) {
				return res;
			}

			// 카드번호가 없다
			if (StringUtils.isEmpty(ocbMap.getCrdNo())) {
				// ocb 카드번호 생성
				ocbMap.setCrdNo(genCrdNo());
				ocbMap.setOcbStatus(PGConstants.PG_OCB_NO_CRD_ADDED);
			}

			// 카드 등록이 아직 안되었다!
			if (PGConstants.PG_OCB_NO_CRD_ADDED.equals(ocbMap.getOcbStatus())) {
				// ocb에 okx 카드 등록
				nxMile.addCard(uard.getUserKey(), ocbMap.getCrdNo());
				ocbMap.setOcbStatus(PGConstants.PG_OCB_NO_CRD_COMBINED);
			}

			// 카드 등록이 아직 안되었다!
			if (PGConstants.PG_OCB_NO_CRD_COMBINED.equals(ocbMap.getOcbStatus())) {
				// ocb에 합산사용 등록
				nxMile.addCombinedUseCard(uard.getUserKey(), ocbMap.getCrdNo());
				ocbMap.setOcbStatus(PGConstants.PG_OKX_STATUS_ON);
			}

			// db에 ocb 매핑 정보 저장
			int rows = ocbMapper.insertUpdateOCBMap(ocbMap);

		} catch (NxMileException nxe) {
			if ("M480".equals(nxe.getBindingFieldByKey("msgGrpCd"))) {
				ocbMap.setOcbStatus(PGConstants.PG_OCB_NO_CRD_ADDED);
				res.setStatus(PGConstants.PG_OCB_NO_CRD_ADDED);
			} else {
				ocbMap.setOcbStatus(PGConstants.PG_OCB_NO_CRD_COMBINED);
				res.setStatus(PGConstants.PG_OCB_NO_CRD_COMBINED);
			}

			int rows = ocbMapper.insertUpdateOCBMap(ocbMap);

		} catch (Exception e) {
			throw new SystemException(e.getCause());
		} finally {
			// redis 트렌잭션 종료
			okxUserService.releaseUserAddLock(okxUser.getUserSn());
		}

		return res;
	}

	private String genCrdNo() {
		long crdNo = ocbMapper.getCrdNoSeq();

		return PGConstants.PG_OCB_CRD_PREFIX + Long.toString(crdNo);
	}

	public PayResToNxMile payToNxMile(PayReqToNxMile req) {
		PayResToNxMile res = new PayResToNxMile();
		res.setOcbTx("123456789");
		return res;
	}

	public long getOcbPoint(OcbPointReq req) {

		Verifier verifier = channelService.decodeVerifier(req.getVerifier());

		OCBMapDto ocbMap = ocbMapper.findOcbMapByUserSn(verifier.getUserSn());
		if (ocbMap == null || StringUtils.isEmpty(ocbMap.getCrdNo())
				|| !PGConstants.PG_OKX_STATUS_ON.equals(ocbMap.getOcbStatus())) {
			throw new GeneralBusinessException("OCB 미연동 회원");
		}

		OKXUser user = userMapper.findUserByUserSn(ocbMap.getUserSn());
		if (user == null) {
			throw new GeneralBusinessException("userSn에 존재 하지 않습니다");
		}

		if (!PGConstants.PG_OKX_STATUS_ON.equals(user.getUserStatus())) {
			throw new GeneralBusinessException("계정이 잠겨 있습니다");
		}

		return nxMile.searchOcbPoint(user.getUserKey(), ocbMap.getCrdNo());

	}

}
