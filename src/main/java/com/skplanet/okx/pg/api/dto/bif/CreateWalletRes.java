package com.skplanet.okx.pg.api.dto.bif;

import lombok.Data;

@Data
public class CreateWalletRes {
	private String address;
	private String secretKey;
}
