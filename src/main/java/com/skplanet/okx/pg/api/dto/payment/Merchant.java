package com.skplanet.okx.pg.api.dto.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "매장정보")
@Data
public class Merchant {

	@ApiModelProperty(value = "매장의 고객번호", required = true, example = "1234")
	private long userSn;

	@ApiModelProperty(value = "디스플레이할 매장명", required = true, example = "좋은매장")
	private String name;
}
