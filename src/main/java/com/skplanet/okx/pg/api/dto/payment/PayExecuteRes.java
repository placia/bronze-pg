package com.skplanet.okx.pg.api.dto.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(description = "자금 거래 실행의 입력 파라미터")
@Data
public class PayExecuteRes {

	@ApiModelProperty(value = "블록체인 트랜젝션 어드레스", required = true, example = "asdf")
	private String transactionAddress;

	
}