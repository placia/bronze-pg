package com.skplanet.okx.pg.api.dto.user.verifier;

import lombok.Data;

@Data
public class Verifier {
	private String chId;
	private String chKey;
	private long userSn;
}
