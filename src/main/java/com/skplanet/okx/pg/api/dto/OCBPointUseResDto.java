package com.skplanet.okx.pg.api.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OCBPointUseResDto {
    private TxResType resultType;
    private String resultCode;
    private String resultMsg;
    private String trackingNo;
    private long txOcbPoint;
    private long discountPoint;
    private long balance;
    private String approvalNo;
    private LocalDateTime approvalDate;
}
