package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFM480 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멥버쉽프로그램ID(A:OKCashbag)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "요청구분(M1:회원+카드발급(무조건 발급) " +
            "M2:회원+카드발급(정상카드 없을시 발급) " +
            "M3:회원+카드발급(카드코드 해지후 발급) " +
            "M4 :회원+카드등록 " +
            "U1:회원+가족정보 변경)")
    private String requestType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "검색조건 (1:회원ID, 2:회원구분번호,  4 :CI)")
    private String searchCondition = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "회원ID(Space)")
    private String memberId = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원구분 (1:일반 2:법인 3:개인사업자 4:패밀리 5:단체)")
    private String memberType = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "회원구분번호 (/법인No/개인사업자No/패밀리No/단체No)")
    private String memberTypeNo = "";
    @NxmileColumn(size = 3, paddingBefore = false)
    @NxmileComment(value = "딜러구분(Space)")
    private String dealerType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "법인사업자번호(Space)")
    private String corpBizNo = "";
    @NxmileColumn(size = 16, paddingBefore = false)
    @NxmileComment(value = "카드번호FALSE")
    private String cardNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "카드상태(A:정상, R:사용SVC제한,F:서비스중지/분실/파손)")
    private String cardStatus = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "카드발급일자(카드발급일자)")
    private String cardIssueDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "제휴사코드FALSE")
    private String partnerCd = "";
    @NxmileColumn(size = 4, paddingBefore = false)
    @NxmileComment(value = "제휴사카드코드FALSE")
    private String partnerCardCd = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "발급가맹점코드FALSE")
    private String issueMerchantCd = "";
    @NxmileColumn(size = 88, paddingBefore = false)
    @NxmileComment(value = "CIFALSE")
    private String ci = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "DIFALSE")
    private String di = "";
    @NxmileColumn(size = 16, paddingBefore = false)
    @NxmileComment(value = "기등록카드번호FALSE")
    private String registeredCardNo = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "한글성명FALSE")
    private String korName = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "영문성명FALSE")
    private String engName = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "실명확인구분(1:실명 " +
            "2:자료없음이나 실명과 유사 " +
            "3:자료없음 , 4:비실명 " +
            "5:성명정보filtering대상 " +
            "6:주민번호로직에 맞지않음(외국인/법인등))")
    private String realNameCheckType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원상태 (A:정상 U:탈회신청 D:탈회확정 S:직권정지)")
    private String memberStatus = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "행정상 회원생년월일(YYYYMMDD )")
    private String adminstMemberBirthYMD = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "회원생년월일(YYYYMMDD)")
    private String memberBirthYMD = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원생일구분(S:양력, M:음력)")
    private String memberBirtType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원성별구분(M/F (개인이외 회원 : ‘E’ ))")
    private String memberGenderType = "";
    @NxmileColumn(size = 4, paddingBefore = false)
    @NxmileComment(value = "자료원천코드(기관코드 )")
    private String dataOrgCd = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "DM발송지구분(H:자택 O:직장)")
    private String dmSenderType = "";
    @NxmileColumn(size = 7, paddingBefore = false)
    @NxmileComment(value = "자택우편번호FALSE")
    private String homeZipCd = "";
    @NxmileColumn(size = 100, paddingBefore = false)
    @NxmileComment(value = "자택주소1FALSE")
    private String homeAddress1 = "";
    @NxmileColumn(size = 100, paddingBefore = false)
    @NxmileComment(value = "자택주소2FALSE")
    private String homeAddress2 = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler(50)")
    private String filler = "";
    @NxmileColumn(size = 7, paddingBefore = false)
    @NxmileComment(value = "직장우편번호FALSE")
    private String officeZipCd = "";
    @NxmileColumn(size = 100, paddingBefore = false)
    @NxmileComment(value = "직장주소1FALSE")
    private String officeAddress1 = "";
    @NxmileColumn(size = 100, paddingBefore = false)
    @NxmileComment(value = "직장주소2FALSE")
    private String officeAddress2 = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler1FALSE")
    private String filler1 = "";
    @NxmileColumn(size = 14, paddingBefore = false)
    @NxmileComment(value = "자택전화번호FALSE")
    private String homeTelNo = "";
    @NxmileColumn(size = 14, paddingBefore = false)
    @NxmileComment(value = "직장전화번호FALSE")
    private String officeTelNo = "";
    @NxmileColumn(size = 14, paddingBefore = false)
    @NxmileComment(value = "휴대폰번호FALSE")
    private String mobileTelNo = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "OCail주소FALSE")
    private String oCallAddress = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "자택전화정확구분(O/X/U)")
    private String homeTelCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "자택주소정확구분(O/X/U)")
    private String homeAddressCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "직장전화정확구분(O/X/U)")
    private String officeTelCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "직장주소정확구분(O/X/U)")
    private String officeAddressCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "휴대폰정확구분(O/X/U)")
    private String mobilePhoneCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "e메일정확구분(O/X/U)")
    private String eMailCorrectType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "본인인증여부(Y/N   삭제)")
    private String selfAuthYN = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "본인인증수단(1 핸드폰, 2 신용카드,3 공인인증, 4 이메일 삭제)")
    private String selfAuthMthd = "";
    @NxmileColumn(size = 98, paddingBefore = false)
    @NxmileComment(value = "Filler2FALSE")
    private String filler2 = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "변경자IDFALSE")
    private String changerId = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "회원변경일자(YYYYMMDD)")
    private String memberChangeDate = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "DM발송제외여부(Y/N)")
    private String dmSendExcptYN = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "개인사업자 환급용계좌실명번호FALSE")
    private String personBizRtnAcctRealNameNo = "";
    @NxmileColumn(size = 20, paddingBefore = false)
    @NxmileComment(value = "개인사업자 환급용계좌번호FALSE")
    private String personBizRtnAcctNo = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "개인사업자 조회용법인등록번호FALSE")
    private String personBizSrchCorpRegNo = "";
    @NxmileColumn(size = 4, paddingBefore = false)
    @NxmileComment(value = "법정대리인 기관코드(법정대리인유입기관코드)")
    private String legalRepresentInputOrgCd = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "법정대리인 성명(법정대리인한글성명)")
    private String legalRepresentKorName = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "법정대리인 생년월일(YYYYMMDD)")
    private String legalRepresentBirthYMD = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "법정대리인 성별(M/F)")
    private String legalRepresentGender = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "법정대리인 인증구분(인증구분 (P) 혹은 (E))")
    private String legalRepresentAuthType = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "법정대리인 인증값(전화번호 혹은 e-메일주소)")
    private String legalRepresentAuthValue = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "법정대리인 관계(한글로 입력(예)부/모)")
    private String legalRepresentRelation = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "법정대리인 동의일자(날짜 유형(YYYYMMDD))")
    private String legalRepresentAgreeYMD = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "법정대리인 동의시간(시간 유형(HH24MISS))")
    private String legalRepresentAgreeTime = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "법정대리인 증명수단(증명수단)")
    private String legalRepresentVrfyMthd = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽 등급 대분류(멤버쉽 등급코드 (대분류))")
    private String membershipGradeLCls = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽 등급 소분류(멤버쉽 등급코드 (소분류))")
    private String membershipGradeSCls = "";
    @NxmileColumn(size = 4, paddingBefore = false)
    @NxmileComment(value = "카드난수FALSE")
    private String cardRandomNum = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "캐쉬백 회원등급(정회원(01),준회원(02))")
    private String ocbMemberGrade = "";
    @NxmileColumn(size = 84, paddingBefore = false)
    @NxmileComment(value = "Filler3FALSE")
    private String filler3 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "Load_seqFALSE")
    private Long loadSeq = 0L;
    @NxmileColumn(size = 28, paddingBefore = false)
    @NxmileComment(value = "Message1FALSE")
    private String message1 = "";
    @NxmileColumn(size = 28, paddingBefore = false)
    @NxmileComment(value = "Message2FALSE")
    private String message2 = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "웹서비스호출구분(내부용(N:framework호출, S:servlet호출, W:WebT))")
    private String webSvcCallType = "";
    @NxmileColumn(size = 3, value = "0", repeat = true)
    private Long gridCnt2 = 0L;
    @NxmileColumn(size = 57, doPadding = false)
    private List<Sub2> sub1 = new ArrayList<>();
    @NxmileColumn(size = 3, value = "0", repeat = true)
    private Long gridCnt3 = 0L;
    @NxmileColumn(size = 22, doPadding = false)
    private List<Sub3> sub2 = new ArrayList<>();

    @Data
    public static class Sub2 {
        @NxmileColumn(size = 8)
        private String agreeVersionCd = "";
        @NxmileColumn(size = 8)
        private String agreeDate = "";
        @NxmileColumn(size = 6)
        private String agreeTime = "";
        @NxmileColumn(size = 1)
        private String agreeYN = "";
        @NxmileColumn(size = 10)
        private String agreeInputChannelCd = "";
        @NxmileColumn(size = 4)
        private String corpCd = "";
        @NxmileColumn(size = 20)
        private String agreeEtcInfoFiller = "";
    }

    @Data
    public static class Sub3 {
        @NxmileColumn(size = 2)
        private String membershipType = "";
        @NxmileColumn(size = 8)
        private String effectiveStartDate = "";
        @NxmileColumn(size = 8)
        private String effectiveEndDate = "";
        @NxmileColumn(size = 1)
        private String membershipSvcStatus = "";
    }

    public NxMileIFM480(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFM480 of() {
        return NxMileIFM480.of(null);
    }

    public static NxMileIFM480 of(String resCode) {
        return new NxMileIFM480("M480", resCode);
    }

    public NxMileIFM480 defaultValue() {
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        return setGridCnt0(1L)
                .setMembershipPgmId("A")
                .setRequestType(ofEmpty(getRequestType(), "M4"))
                .setSearchCondition("4")
                .setMemberType("1")
                .setCardIssueDate(date)
                .setMemberStatus("A")
                .setChangerId(getCorpCd())
                .setDataOrgCd(getCorpCd()) // 자료원천코드
                .setMemberChangeDate(date)
                .build();
    }
}
