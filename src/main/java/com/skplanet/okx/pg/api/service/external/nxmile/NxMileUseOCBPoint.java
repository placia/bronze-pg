package com.skplanet.okx.pg.api.service.external.nxmile;

import com.skplanet.okx.pg.api.dto.OCBPointUseReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointUseResDto;
import com.skplanet.okx.pg.api.dto.TxResType;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK400;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK401;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileResultCode;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import com.skplanet.okx.pg.support.exception.impl.NxMileNetCancelException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class NxMileUseOCBPoint extends NxMileIFExecutor<OCBPointUseReqDto, OCBPointUseResDto> {
    public NxMileUseOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public OCBPointUseResDto execute(OCBPointUseReqDto pointReqDto) {
        NxMileIFK400 request = getNxMileIFK400Req(pointReqDto);
        NxMileIFK401 response;

        try {
            response = nxMileConn.sendWithTryAndReceive(request, NxMileIFK401.class,
                    2,
                    req -> req.setResCdLcls("60"),	// 재전송시엔 응답코드를 "60" 으로 Setting
                    resp -> NxMileResultCode.TIME_OVER.isEquals(resp.getResultCode()));	// 응답값이 9999인 경우 1회 재전송
        } catch (NxMileException e) {
            log.error("OCB포인트 사용요청 오류:{}", request.getTrackingNo());
            throw e;
        }

        OCBPointUseResDto pointResDto;

        try {
//			아래 케이스인 경우 망취소 필요. NxMileNetCancelException throw
//				SYSTEM_ERROR1("9080", "시스템실연락바람")
//				SYSTEM_ERROR2("9966", "시스템실연락바람")
//				TIME_OVER("9999", "시간초과재시도요망")
//			아닌경우 NxMIleException throw

            failHandleService.test(request, response);

            pointResDto = getOcbPointUseResForSuccess(response);
        } catch (NxMileNetCancelException e) {
            log.error("망취소 요청처리 필요:{}", request.getTrackingNo(), e);

            //TODO: 이 메소드를 호출하는 서비스는 NETWORK_ERROR인 경우 망취소 처리가 필요하다.
            pointResDto = getOcbPointUseResForFailure(response, TxResType.NETWORK_ERROR, "망취소");
        } catch (NxMileException e) {
            log.error("OCB포인트 사용요청 실패:{}:{}", request.getTrackingNo(), response.getResultCode(), e);
            pointResDto = getOcbPointUseResForFailure(response, TxResType.FAIL, response.getMessage1());
        }

        return pointResDto;
    }

    private OCBPointUseResDto getOcbPointUseResForSuccess(NxMileIFK401 response) {
        OCBPointUseResDto pointResDto = new OCBPointUseResDto();
        pointResDto.setResultType(TxResType.SUCCESS);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg("SUCCESS");
        pointResDto.setTrackingNo(response.getTrackingNo());
        pointResDto.setTxOcbPoint(response.getUsablePoint1());
        pointResDto.setDiscountPoint(Long.parseLong(StringUtils.defaultIfEmpty(response.getMessage3(), "0")));
        pointResDto.setApprovalNo(response.getApproveNo());
        pointResDto.setApprovalDate(LocalDateTime.parse(response.getApproveDate()+response.getApproveTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        return pointResDto;
    }

    private OCBPointUseResDto getOcbPointUseResForFailure(NxMileIFK401 response, TxResType resultType, String resultMsg) {
        OCBPointUseResDto pointResDto = new OCBPointUseResDto();
        pointResDto.setResultType(resultType);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg(resultMsg);
        pointResDto.setTrackingNo(response.getTrackingNo());
        return pointResDto;
    }

    private NxMileIFK400 getNxMileIFK400Req(OCBPointUseReqDto pointReqDto) {
        return (NxMileIFK400) NxMileIFK400.of()
                .setMerchantNo(pointReqDto.getMctNo())
                .setMerchantBizNo(pointReqDto.getBizNo())
                .setTrxDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(pointReqDto.getTxDate()))
                .setTrxTime(DateTimeFormatter.ofPattern("HHmmss").format(pointReqDto.getTxDate()))
                .setTrackData(pointReqDto.getCrdNo())
                .setSlipCd("11")
                .setTrxTypeCd("40")
                .setComplexPayYn("N")
                .setTrxAmtTotSum(pointReqDto.getTotalAmt())
                .setContractTypeCd1("08")
                .setTrxAmt1(pointReqDto.getTxOcbPoint())
                .setPassword("            FUSE")
                .setUsageType(0L)
                .setCashReceiptProcType(1L)
                .setWebSvcCallType("N")
                .setTrackingNo(pointReqDto.getTrackingNo());
    }
}
