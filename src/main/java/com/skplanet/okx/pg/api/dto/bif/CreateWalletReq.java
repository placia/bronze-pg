package com.skplanet.okx.pg.api.dto.bif;

import lombok.Data;

@Data
public class CreateWalletReq {
	private long userSn;
}
