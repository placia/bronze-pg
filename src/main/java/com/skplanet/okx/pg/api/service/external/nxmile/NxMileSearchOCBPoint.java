package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFW400;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class NxMileSearchOCBPoint extends NxMileIFExecutor<NxMileSearchOCBPoint.Request, Long> {
    public NxMileSearchOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public Long execute(NxMileSearchOCBPoint.Request searchPointReq) {
        NxMileIFW400 request = NxMileIFW400.of()
                .setCardNo(searchPointReq.getCrdNo())
                .setCi(searchPointReq.getCi());

        NxMileIFW400 response = null;
        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFW400.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .put("ci", searchPointReq.getCi())
                    .putAll(request.valuesForException())
                    .build());
        }

        failHandleService.test(request, response);

        return response.getUsablePoint1();
    }

    @Getter
    @Builder
    public static class Request {
        private String crdNo;
        private String ci;
    }
}
