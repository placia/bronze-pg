package com.skplanet.okx.pg.api.mapper

import com.skplanet.okx.pg.api.model.OKXUser

import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.SelectKey
import org.springframework.transaction.annotation.Transactional


public interface OKXUserMapper {

	@Select("""
		SELECT SEQ_OKX_USERSN.nextval FROM DUAL
	""")
	long genUserSn();
	
	
	@Select("""
		SELECT
			user_sn userSn,
			user_key userKey,
			wallet_key walletKey,
			user_status userStatus
		FROM okx_user
		WHERE user_sn = #{userSn}
	""")
	OKXUser findUserByUserSn(@Param("userSn") long userSn);
	
	
	@Select("""
		SELECT
			user_sn userSn,
			user_key userKey,
			wallet_key walletKey,
			user_status userStatus
		FROM okx_user
		WHERE user_key = #{userKey}
	""")
	OKXUser findUserByUserKey(@Param("userKey") String userKey);

	
	//@SelectKey(statement="SELECT SEQ_OKX_USERSN.nextval FROM DUAL", keyProperty="userSn", before=true, resultType=Long.class)
	@Insert("""
		INSERT INTO okx_user (user_sn, user_key, wallet_key, user_status)
		VALUES (#{userSn}, #{userKey}, #{walletKey}, #{userStatus})
	""")
	@Transactional
	int insertUser(OKXUser user);

	
}
