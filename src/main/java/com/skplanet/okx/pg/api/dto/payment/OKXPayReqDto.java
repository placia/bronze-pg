package com.skplanet.okx.pg.api.dto.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "nxmile로부터 합산사용 요청 받기 입력 파라미터")
@Data
public class OKXPayReqDto {
	@ApiModelProperty(value = "ocb 카드번호", required = true, example = "1111222233334444")
	private String crdNo;
	
	@ApiModelProperty(value = "가맹점번호", required = true, example = "asdf")
	private String shopId;
	
	@ApiModelProperty(value = "합산사용 요청액", required = true, example = "1000")
	private long amount;
}