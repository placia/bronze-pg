package com.skplanet.okx.pg.api.dto.user.add;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "회원 추가 입력 파라미터")
@Data
public class OKXUserAddReqDto {

	@NotNull
	@ApiModelProperty(value = "채널 아이디", required = true, example = "your_ch_id")
	private String chId;

	@NotNull
	@ApiModelProperty(value = "채널 암호키", required = true, example = "your_ch_key")
	private String chKey;
	
	@NotNull
	@ApiModelProperty(value = "88자 CI", required = true, example = "ci1234...")
	private String userKey;

}
