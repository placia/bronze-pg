package com.skplanet.okx.pg.api.service;


import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.skplanet.okx.pg.api.dto.user.verifier.Verifier;
import com.skplanet.okx.pg.api.mapper.ChannelMapper;
import com.skplanet.okx.pg.api.mapper.OKXUserMapper;
import com.skplanet.okx.pg.api.model.OKXUser;
import com.skplanet.okx.pg.constant.PGConstants;
import com.skplanet.okx.pg.support.exception.impl.GeneralBusinessException;
import com.skplanet.okx.pg.support.exception.impl.InvalidParamException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChannelService {

	@Autowired
	ChannelMapper channelmapper;
	
	@Autowired
	OKXUserMapper userMapper;
	
	@Autowired
    private RedisTemplate<String, Object> redisTemplate;
	
	
	public String genVerifier(String chId, String chKey, long userSn) {

		// 채널정보 유효성 확인
		validateChannel(chId, chKey);
		
		
		// 사용자가 정상인지 확인
		OKXUser user = userMapper.findUserByUserSn(userSn);
		if (user == null) {
			throw new GeneralBusinessException("userSn에 존재 하지 않습니다");
		}
		
		if (!PGConstants.PG_OKX_STATUS_ON.equals(user.getUserStatus())) {
			throw new GeneralBusinessException("계정이 잠겨 있습니다");
		}
		
		
		// verifier 만들기
		String vkey = DigestUtils.md5DigestAsHex((chId + chKey + userSn).getBytes());
		
		Verifier verifier = new Verifier();
		verifier.setChId(chId);
		verifier.setChKey(chKey);
		verifier.setUserSn(userSn);
		
		
		// redis에 verifier 저장하기, 3분간 유효
		redisTemplate.opsForValue().set(getVerifierKey(vkey), verifier, 180, TimeUnit.SECONDS);
		
		
		return vkey;
	}
	
	private String getVerifierKey(String vkey) {
		return PGConstants.REDIS_USER_VERIFIER_KEY_PREFIX + vkey;
	}
	

	// 채널이 정상인지 확인
	public void validateChannel(String chId, String chKey) {
		
		String chStatus = channelmapper.getChStatus(chId, chKey);
		if (StringUtils.isEmpty(chStatus)) {
			throw new GeneralBusinessException("chId, chKey가 일치하지 않습니다");
		}
		
		if (!PGConstants.PG_OKX_STATUS_ON.equals(chStatus)) {
			throw new GeneralBusinessException("채널이 잠겨 있습니다");
		}
	}
	
	
	// verifier string으로 부터 요청자 정보 구하기
	public Verifier decodeVerifier(String vkey) {
		
		Verifier verifier = (Verifier) redisTemplate.opsForValue().get(getVerifierKey(vkey));
		
		if (verifier == null) {
			throw new InvalidParamException("만료된 verifier 입니다");
		}
		
		return verifier;
	}
	
	
	
}
