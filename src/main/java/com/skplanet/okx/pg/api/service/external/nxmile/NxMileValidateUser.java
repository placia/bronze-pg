package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFW400;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class NxMileValidateUser extends NxMileIFExecutor<NxMileValidateUser.Request, Void> {
    public NxMileValidateUser(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public Void execute(NxMileValidateUser.Request validateReq) {
        NxMileIFW400 request = NxMileIFW400.of()
                .setCi(validateReq.getCi());

        NxMileIFW400 response = null;
        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFW400.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .put("ci", validateReq.getCi())
                    .putAll(request.valuesForException())
                    .build());
        }

        failHandleService.test(request, response);

        return null;
    }

    @Getter
    @Builder
    public static class Request {
        private String ci;
    }
}
