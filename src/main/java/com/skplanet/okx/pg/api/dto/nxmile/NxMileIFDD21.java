package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD21 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램IDFALSE")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분FALSE")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분FALSE")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호FALSE")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678       ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "주민번호(‘-‘제외)")
    private String ssn = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "승인일자(YYYYMMDD)")
    private String approveDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "승인시간(hhmmss)")
    private String approveTime = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "승인번호(‘123456789‘ 숫자만 사용한다.)")
    private String approveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1FALSE")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용가능한 포인트)")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "실차감포인트1FALSE")
    private Long realSubstractPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명2FALSE")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트2(사용가능한 포인트)")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트2(현재포인트 + 가용포인트)")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "실차감포인트2FALSE")
    private Long realSubstractPoint2 = 0L;
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "현금취소처리여부(→현금 취소처리 안된 이유 코드집 활용)")
    private String cashCancelProcType = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "현금취소오류메시지(현금영수증 오류 관련 메시지)")
    private String cashCancelErrorMessage = "";
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "현금거래취소금액(현금거래금액(공급가액+부가세+봉사료))")
    private Long cashTrxCancelAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "공급가액FALSE")
    private Long splyAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "부가가치세FALSE")
    private Long vat = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "봉사료FALSE")
    private Long serviceAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "용도구분(소비자소득공제용(0:기본값) 사업자지출증빙용(1))")
    private Long usageType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "현금영수증취소사유(1:거래취소,2:오류취소,3:기타)")
    private String cashReceiptCancelRsn = "";

    public NxMileIFDD21(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD21 of() {
        return NxMileIFDD21.of(null);
    }

    public static NxMileIFDD21 of(String resCode) {
        return new NxMileIFDD21("DD21", resCode);
    }

    public NxMileIFDD21 defaultValue() {
        return setGridCnt0(1L)
                .build();
    }
}
