package com.skplanet.okx.pg.api.mapper

import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.SelectKey
import org.apache.ibatis.annotations.Update
import org.springframework.transaction.annotation.Transactional

import com.skplanet.okx.pg.api.model.OCBMapDto


public interface OCBMapper {

	@Select("""
		SELECT SEQ_OCB_CRDNO.nextval FROM DUAL
	""")
	long getCrdNoSeq();
	
	
	@Select("""
		SELECT 
			user_sn userSn
		,	crd_no crdNo
		,	ocb_status ocbStatus
		FROM OCB_MAP WHERE user_sn = #{userSn}
	""")
	OCBMapDto findOcbMapByUserSn(@Param("userSn") long userSn);
	
	
	@Select("""
		SELECT 
			user_sn userSn
		,	crd_no crdNo
		,	ocb_status ocbStatus
		FROM OCB_MAP WHERE crd_no = #{crdNo}
	""")
	OCBMapDto findOcbMapByCrdNo(@Param("crdNo") String crdNo);

	
	@Insert("""<script>
		MERGE INTO ocb_map dst
		USING dual
		ON (dst.user_sn = #{userSn})
		WHEN MATCHED THEN
			UPDATE SET upddt=sysdate
			<if test="!@org.springframework.util.StringUtils@isEmpty(crdNo)">, crd_no = #{crdNo}</if>
			<if test="!@org.springframework.util.StringUtils@isEmpty(ocbStatus)">, ocb_status = #{ocbStatus}</if>
		WHEN NOT MATCHED THEN
			INSERT (user_sn, crd_no, ocb_status)
			VALUES (#{userSn}, #{crdNo}, #{ocbStatus})
	</script>""")
	int insertUpdateOCBMap(OCBMapDto ocbMap);
	

}
