package com.skplanet.okx.pg.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.okx.pg.api.dto.bif.PaymentInfo;
import com.skplanet.okx.pg.api.dto.bif.PaymentResult;
import com.skplanet.okx.pg.api.dto.payment.PayPrepareReq;
import com.skplanet.okx.pg.api.dto.payment.PayPrepareRes;
import com.skplanet.okx.pg.api.dto.payment.PayExecuteReq;
import com.skplanet.okx.pg.api.dto.payment.PayExecuteRes;
import com.skplanet.okx.pg.api.dto.user.verifier.Verifier;

@Service
public class PaymentService {

	@Autowired
	ChannelService channelService;
	
	@Autowired
	BIFService bif;
	
	
	public PayPrepareRes payPrepare(PayPrepareReq req) {
		PayPrepareRes res = new PayPrepareRes();
		
		// verifier를 검증한다.
		Verifier verifier = channelService.decodeVerifier(req.getVerifier());
		
		// BIF에 구매를 위한 사전 요청을 질의한다. (매장정보, 사용자 잔액 확인)
		PaymentInfo paymentInfo = bif.getPaymentInfo(verifier.getUserSn(), req.getTargetUserSn(), req.getPrice());
		
		res.setMerchant(paymentInfo.getMerchant());
		res.setDappBalanceList(paymentInfo.getBalances());
		
		return res;
	}
	
	
	public PayExecuteRes payExecute(PayExecuteReq req) {
		PayExecuteRes res = new PayExecuteRes();
		
		// verifier를 검증한다.
		Verifier verifier = channelService.decodeVerifier(req.getVerifier());
		
		// BIF에 실제 구매 요청
		PaymentResult pr = bif.executePayment(verifier.getUserSn(), req.getTargetUserSn(), req.getPrice()
											, req.getClientTx(), req.getClientJson(), req.getTokenAmountMap());
				
		
		// 응답
		res.setTransactionAddress(pr.getTransactionAddress());
		
		return res;
	}
	

}
