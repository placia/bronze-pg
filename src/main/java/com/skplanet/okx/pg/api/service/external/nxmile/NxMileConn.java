package com.skplanet.okx.pg.api.service.external.nxmile;

import com.skplanet.ocb.tcp.domain.EncodeBuilder;
import com.skplanet.ocb.tcp.netty.TcpClient;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFHeader;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Service
@Slf4j
@Setter
@Accessors(chain = true)
public class NxMileConn {
    @Value("${tcp.nxmile.host}")
    private String host;

    @Value("${tcp.nxmile.port}")
    private int port;

    @Value("${tcp.charset}")
    private String charset;

    @Value("${tcp.nxmile.sotimeout:3}")
    private int soTimeoutSecs;

    @Value("${tcp.nxmile.readtimeout:3}")
    private int readTimeoutSecs;

    /**
     * NxMile 송수신
     * @param request NxMile 요청전문 객체
     * @param classOfReturn NxMile 응답 클래스
     * @return
     * @throws IOException
     */
    public <T extends EncodeBuilder, R> R sendAndReceive(T request, Class<R> classOfReturn) throws IOException {
        return TcpClient.of(host, port, charset)
                .soTimeoutSecs(soTimeoutSecs)
                .readTimeoutSecs(readTimeoutSecs)
                .connet()
                .sendAndReceive(request, classOfReturn);
    }

    /**
     *
     * @param request NxMile 요청전문 객체
     * @param classOfReturn NxMile 응답 클래스
     * @param maxAttempts try 최대 횟수
     * @param retryModifier retry 처리할 때 요청전문 객체가 수정되는 내용
     * @param retryTests IOException 외 응답코드에서 retry로 판단해야 하는 코드 확인 리스트
     * @return
     */
    public <T extends NxMileIFHeader, R extends NxMileIFHeader> R sendWithTryAndReceive(T request, Class<R> classOfReturn,
                                                                int maxAttempts,
                                                                Consumer<NxMileIFHeader> retryModifier,
                                                                Predicate<R>... retryTests) {
        NxMileRetryHandler<T, R> retryHandler = new NxMileRetryHandler<>(
                TcpClient.of(host, port, charset).soTimeoutSecs(soTimeoutSecs).readTimeoutSecs(readTimeoutSecs),
                maxAttempts, retryTests
        );

        return retryHandler.perform(request, classOfReturn, retryModifier).get();
    }
}
