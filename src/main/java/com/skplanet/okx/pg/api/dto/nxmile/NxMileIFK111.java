package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFK111 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램ID(A:OKCashbag)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분(코드값 참조)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분(단말기구분코드 참조)")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호(CAT ID)")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678       ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "회원구분번호(법인번호 또는 개인사업자번호)")
    private String memberTypeNo = "";
    @NxmileColumn(size = 88, paddingBefore = false)
    @NxmileComment(value = "CI(CI(신규))")
    private String ci = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler2(여유분(신규))")
    private String filler1 = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "회원ID(NXM회원ID(신규))")
    private String memberId = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "승인일자(YYYYMMDD)")
    private String approveDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "승인시간(hhmmss)")
    private String approveTime = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "승인번호FALSE")
    private String approveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1FALSE")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용가능한 포인트)")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "실차감포인트1(대외기관사용)")
    private String realSubstractPoint1 = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1FALSE")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용가능한 포인트)")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "실차감포인트1(대외기관사용)")
    private String realSubstractPoint2 = "";
    @NxmileColumn(size = 2)
    @NxmileComment(value = "현금취소처리여부FALSE")
    private String cashCancelProcType = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "현금취소오류메시지FALSE")
    private String cashCancelErrorMessage = "";
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "현금거래취소금액FALSE")
    private Long cashTrxCancelAmt = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "공급가액FALSE")
    private String splyAmt = "";
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "부가가치세FALSE")
    private Long vat = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "봉사료FALSE")
    private String serviceAmt = "";
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "용도구분FALSE")
    private Long usageType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "웹서비스호출구분FALSE")
    private String webSvcCallType = "";
    @NxmileColumn(size = 50, paddingBefore = false)
    @NxmileComment(value = "Filler(여유분(신규))")
    private String filler2 = "";
}
