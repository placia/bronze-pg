package com.skplanet.okx.pg.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skplanet.okx.pg.api.dto.user.verifier.VerifierReq;
import com.skplanet.okx.pg.api.dto.user.verifier.VerifierRes;
import com.skplanet.okx.pg.api.service.ChannelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "채널용 API")
@RestController
@RequestMapping(value = "/channel")
public class ChannelController {

	@Autowired
	private ChannelService channelService;
	
	
	@ApiOperation("사용자 토큰 요청")
	@PostMapping(value="/verifier")
	public VerifierRes genVerifier(@Valid @RequestBody VerifierReq req) {
		String verifier = channelService.genVerifier(req.getChId(), req.getChKey(), req.getUserSn());
		
		VerifierRes res = new VerifierRes();
		res.setVerifier(verifier);
		
		return res;
	}
	
	
}
