package com.skplanet.okx.pg.api.dto.payment;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "OCB point 조회 요청 파라미터")
@Data
public class OcbPointReq {
	
	@NotNull
	@ApiModelProperty(value = "verifier", required = true, example = "aaaa")
	private String verifier;

}
