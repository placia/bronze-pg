package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.ocb.tcp.netty.TcpClient;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFHeader;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import com.skplanet.okx.pg.support.exception.impl.NxMileRetryException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Slf4j
@Accessors(chain = true)
public class NxMileRetryHandler<T extends NxMileIFHeader, R extends NxMileIFHeader> {
    private final int maxAttempts;

    /** 재시도 요청시 delay milliseconds */
    @Setter
    private long delay = 0L;
    private AtomicInteger attempts;
    private final Predicate<R> retryTests;
    /** 발생한 오류 */
    @Getter private final List<Throwable> errors;

    private final TcpClient tcpClient;

    /**
     * NxMile 재시도 요청 핸들러
     *
     * @param tcpClient 연동 클라이언트
     * @param maxAttempts 최대 재시도 횟수
     * @param retryTests 재시도 check predicate
     */
    public NxMileRetryHandler(
            TcpClient tcpClient,
            int maxAttempts,
            Predicate<R>... retryTests
    ) {
        this.maxAttempts = maxAttempts;
        this.attempts = new AtomicInteger(0);
        this.retryTests = Arrays.asList(retryTests).stream().reduce(Predicate::or).orElse(x -> false);
        this.errors = new ArrayList<>();
        this.tcpClient = tcpClient;
    }

    /**
     * 마지막 요청 응답값이 존재하는 경우 응답값을 리턴, 만약 마지막 요청 응답값이 존재하지 않는 경우 NxMileException throw
     * 발생한 모든 요청에 대한 Throwable은 getError로 조회가능함.
     *
     * @param request
     * @param classOfReturn 응답 class
     * @return
     * @throws NxMileException
     */
    public Optional<R> perform(T request, Class<R> classOfReturn, Consumer<NxMileIFHeader> consumer) throws NxMileException {
        R response = null;

        for (; ; ) {
            try {
                response = this.tcpClient.connet().sendAndReceive(request, classOfReturn);
                if (retryTests.test(response)) {
                    throw new NxMileRetryException(String.format("[%s]", response.getResultCode()));
                }

                return Optional.of(response);
            } catch (IOException | NxMileRetryException e) {
                errors.add(e);
                consumer.accept(request);

                if (attempts.incrementAndGet() < maxAttempts) {
                    response = null;
                    log.warn("재시도 요청 {}:{}", request.getMsgGrpCd(), request.getTrackingNo());
                } else {
                    log.error("[NXMILE 연동오류] 재시도({}) 실패 {}:{}", attempts.get(), request.getMsgGrpCd(), request.getTrackingNo());
                    if (response != null) return Optional.ofNullable(response);

                    throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                            .putAll(request.valuesForException())
                            .build());
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(delay);
            } catch (InterruptedException e) {
            }
        }
    }
}
