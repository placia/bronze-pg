package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFA960;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class NxMileAddCombinedCard extends NxMileIFExecutor<NxMileAddCombinedCard.Request, Void> {
    public NxMileAddCombinedCard(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public Void execute(NxMileAddCombinedCard.Request addCombinedCardReq) {
        NxMileIFA960 request = NxMileIFA960.of()
                .setCardNo(addCombinedCardReq.getCrdNo());

        NxMileIFA960 response = null;
        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFA960.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .put("ci", addCombinedCardReq.getCi())
                    .put("crdNo", addCombinedCardReq.getCrdNo())
                    .putAll(request.valuesForException())
                    .build());
        }

        failHandleService.test(request, response);
        return null;
    }

    @Getter
    @Builder
    public static class Request {
        private String ci;
        private String crdNo;
    }
}
