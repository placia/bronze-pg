package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD20 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0L(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램IDFALSE")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분FALSE")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분FALSE")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호FALSE")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0L:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678       ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가맹점사업자번호FALSE")
    private Long merchantBizNo = 0L;
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "주민번호(‘-‘제외)")
    private String ssn = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "제휴사 가맹점명FALSE")
    private String partnerMerchantName = "";
    @NxmileColumn(size = 25, paddingBefore = false)
    @NxmileComment(value = "제휴사 가맹점번호FALSE")
    private String partnerMerchantNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "제휴사 승인번호FALSE")
    private String partnerApproveNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "원거래일자(YYYYMMDD, 항목설명 참조)")
    private String orgTrxDate = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "원거래승인번호FALSE")
    private String orgApproveNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "원거래제휴사승인번호FALSE")
    private String orgTrxPartnerApproveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "전표코드(22)")
    private String slipCd = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "거래구분코드(42)")
    private String trxTypeCd = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "복합결제여부(N)")
    private String complexPayYN = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "원거래금액(실사용 금액)")
    private Long orgTrxAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "취소 요청구분(‘0L’:적립/할인+현금취소, ‘1’:적립/할인Only취소)")
    private Long cancelRequestType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "현금영수증취소사유(1:거래취소,2:오류취소,3:기타)")
    private String cashReceiptCancelRsn = "";

    public NxMileIFDD20(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD20 of() {
        return NxMileIFDD20.of(null);
    }

    public static NxMileIFDD20 of(String resCode) {
        return new NxMileIFDD20("DD20", resCode);
    }

    public NxMileIFDD20 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId("A")
                .setMembershipSvcType("A1")
                .build();
    }
}
