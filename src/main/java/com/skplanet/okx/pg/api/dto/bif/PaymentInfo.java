package com.skplanet.okx.pg.api.dto.bif;

import java.util.ArrayList;
import java.util.List;

import com.skplanet.okx.pg.api.dto.payment.DappBalance;
import com.skplanet.okx.pg.api.dto.payment.Merchant;

import lombok.Data;

@Data
public class PaymentInfo {
	private Merchant merchant;
	private List<DappBalance> balances = new ArrayList<>();
	
}
