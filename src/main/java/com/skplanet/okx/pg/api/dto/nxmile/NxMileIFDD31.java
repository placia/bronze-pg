package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD31 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(1:전체 DATA건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멥버쉽프로그램ID(A:OKCashbag)")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분(A1)")
    private String membershipSvcType = "";
    @NxmileColumn(size = 3, paddingBefore = false)
    @NxmileComment(value = "포인트종류FALSE")
    private String pointType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분FALSE")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호FALSE")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "제휴사코드FALSE")
    private String partnerCd = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(가맹점번호)")
    private String merchantNo = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가맹점사업자번호FALSE")
    private Long merchantBizNo = 0L;
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "주민번호FALSE")
    private String ssn = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "회원한글명FALSE")
    private String memberKorName = "";
    @NxmileColumn(size = 16, paddingBefore = false, trim = false)
    @NxmileComment(value = "비밀번호FALSE")
    private String password = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "요청구분(RD)")
    private String requestType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "회원구분(1:일반 2:법인 3:개인사업자 4:패밀리 5:단체)")
    private String memberType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "실명확인구분(1:실명 " +
            "2:자료없음이나실명과유사 " +
            "3:자료없음 " +
            "4:비실명 " +
            "5:성명정보filtering대상" +
            "6:주민번호로직에맞지않음(외국인/법인등))")
    private String realNameCheckType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "적립가능여부(Y/N)")
    private String isAccumableType = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "사용가능여부(Y/N)")
    private String isUsableType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1FALSE")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "발생포인트1FALSE")
    private Long occrPoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용가능한 포인트)")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명2FALSE")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "발생포인트2FALSE")
    private Long occrPoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트2(사용가능한 포인트)")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트2(현재포인트 + 가용포인트)")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "선택P/W보유여부(Y/N)")
    private String selpasswdPossessType = "";
    @NxmileColumn(size = 17, paddingBefore = false)
    @NxmileComment(value = "Filler((내부용 회원ID I/F))")
    private String filler = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message_4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "웹서비스호출구분FALSE")
    private String webSvcCallType = "";

    public NxMileIFDD31(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD31 of() {
        return NxMileIFDD31.of(null);
    }

    public static NxMileIFDD31 of(String resCode) {
        return new NxMileIFDD31("DD31", resCode);
    }

    public NxMileIFDD31 defaultValue() {
        return setGridCnt0(1L)
                .build();
    }
}
