package com.skplanet.okx.pg.api.dto.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Dapp들의 토큰별 잔액")
@Data
public class DappBalance {

	@ApiModelProperty(value = "dapp 주소", required = true, example = "asdf...")
	private String dappAddress;

	@ApiModelProperty(value = "display할 dapp의 이름", required = true, example = "sk토큰")
	private String dappName;

	@ApiModelProperty(value = "해당 dapp에서 쓸 수 있는 잔액", required = true, example = "100")
	private long balance;
	
	public DappBalance() {}
	
	public DappBalance(String dappAddress, String dappName, long balance) {
		this.dappAddress = dappAddress;
		this.dappName = dappName;
		this.balance = balance;
	}
}
