package com.skplanet.okx.pg.api.service;

import com.skplanet.okx.pg.api.dto.OCBPointCancelReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointCancelResDto;
import com.skplanet.okx.pg.api.dto.OCBPointUseReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointUseResDto;
import com.skplanet.okx.pg.api.service.external.nxmile.*;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Setter
public class NxMileIFService {
	@Autowired
	private NxMileFailHandleService predicatesService;

	@Autowired
	private NxMileConn nxMileConn;

	/**
	 * 회원가입여부확인(W400)
	 * @param ci
	 * @return
	 */
	public void validateOcbUser(String ci) {
		NxMileIFExecutor<NxMileValidateUser.Request, Void> executor = new NxMileValidateUser(predicatesService, nxMileConn);
		executor.execute(NxMileValidateUser.Request.builder().ci(ci).build());
	}
	

	/**
	 * 카드등록(M480)
	 * @param ci
	 * @param crdNo
	 */
	public void addCard(String ci, String crdNo) {
		NxMileIFExecutor<NxMileAddCard.Request, Void> executor = new NxMileAddCard(predicatesService, nxMileConn);
		executor.execute(NxMileAddCard.Request.builder().ci(ci).crdNo(crdNo).build());
	}
	

	/**
	 * 합산사용등록(A960)
	 * @param ci
	 * @param crdNo
	 */
	public void addCombinedUseCard(String ci, String crdNo) {
		NxMileIFExecutor<NxMileAddCombinedCard.Request, Void> executor = new NxMileAddCombinedCard(predicatesService, nxMileConn);
		executor.execute(NxMileAddCombinedCard.Request.builder().ci(ci).crdNo(crdNo).build());
	}

	/**
	 * 포인트조회(W400)
	 * @param ci
	 * @param crdNo
	 * @return
	 */
	public long searchOcbPoint(String ci, String crdNo) {
		NxMileIFExecutor<NxMileSearchOCBPoint.Request, Long> executor = new NxMileSearchOCBPoint(predicatesService, nxMileConn);
		return executor.execute(NxMileSearchOCBPoint.Request.builder().ci(ci).crdNo(crdNo).build());
	}

	/**
	 * 포인트사용(K400)
	 * @param pointReqDto
	 * @return
	 */
	public OCBPointUseResDto useOcbPoint(OCBPointUseReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointUseReqDto, OCBPointUseResDto> executor = new NxMileUseOCBPoint(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}

	/**
	 * 포인트사용취소(K410)
	 * @param pointReqDto
	 * @return
	 */
	public OCBPointCancelResDto cancelUseOcbPoint(OCBPointCancelReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> executor = new NxMileCancelUseOCBPoint(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}

	/**
	 * 포인트사용 망취소(K410)
	 * @param pointReqDto
	 * @return
	 */
	public OCBPointCancelResDto cancelUseOcbPointForNwError(OCBPointCancelReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> executor = new NxMileCancelUseOCBPointForNwError(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}

	/**
	 * 포인트충전(K100)
	 * @param pointReqDto
	 * @return
	 */
    public OCBPointUseResDto chargeOcbPoint(OCBPointUseReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointUseReqDto, OCBPointUseResDto> executor = new NxMileChargeOCBPoint(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}

	/**
	 * 포인트충전취소(K110)
	 * @param pointReqDto
	 * @return
	 */
	public OCBPointCancelResDto cancelChargeOcbPoint(OCBPointCancelReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> executor = new NxMileCancelChargeOCBPoint(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}

	/**
	 * 포인트충전 망취소(K110)
	 * @param pointReqDto
	 * @return
	 */
	public OCBPointCancelResDto cancelChargeOcbPointForNwError(OCBPointCancelReqDto pointReqDto) {
		NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> executor = new NxMileCancelChargeOCBPointForNwError(predicatesService, nxMileConn);
		return executor.execute(pointReqDto);
	}
}
