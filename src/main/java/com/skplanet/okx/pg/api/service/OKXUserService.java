package com.skplanet.okx.pg.api.service;


import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.skplanet.okx.pg.api.dto.bif.CreateWalletReq;
import com.skplanet.okx.pg.api.dto.bif.CreateWalletRes;
import com.skplanet.okx.pg.api.dto.user.add.OKXUserAddReqDto;
import com.skplanet.okx.pg.api.mapper.OKXUserMapper;
import com.skplanet.okx.pg.api.model.OKXUser;
import com.skplanet.okx.pg.constant.PGConstants;
import com.skplanet.okx.pg.support.exception.impl.GeneralBusinessException;

import reactor.core.publisher.Mono;

@Service
public class OKXUserService {
	
	@Autowired
	private OKXUserMapper userMapper;
	
	@Autowired
	private ChannelService channelService;

	@Autowired
	private BIFService bif;
	
	@Autowired
	RedisTemplate<String, Object> redis;
	
	
	
	public OKXUser addUser(OKXUserAddReqDto uard) {
	
		// 채널정보 유효성 확인
		channelService.validateChannel(uard.getChId(), uard.getChKey());
		
		
		// 기존 가입 여부 확인
		OKXUser okxUser = userMapper.findUserByUserKey(uard.getUserKey());
		if (okxUser != null) {

			// okx_user.user_status가 0이 아닌 경우, (계정상태가 정상이 아닌 경우)
			if (! PGConstants.PG_OKX_STATUS_ON.equals(okxUser.getUserStatus())) {
				throw new GeneralBusinessException("계정 잠김 상태");
			}
		
			return okxUser;
		}
		long userSn = userMapper.genUserSn();
		
		
		// redis를 이용해 트렌잭션 걸기 userSn으로 제약
		if (!getUserAddLock(userSn)) {
			throw new GeneralBusinessException("기존 가입 요청 처리중 입니다. 잠시후 재시도 바랍니다.");
		}
		
		
		try {
			
			// bif 랑 통신해서 블록체인에 wallet 추가
			CreateWalletRes bifRes = bif.createWallet(userSn);
			
			
			if (okxUser == null) {
				okxUser = new OKXUser();
				okxUser.setUserSn(userSn);
				okxUser.setUserKey(uard.getUserKey());
				okxUser.setWalletKey(bifRes.getSecretKey());
				okxUser.setUserStatus(PGConstants.PG_OKX_STATUS_ON);
	
				int rows = userMapper.insertUser(okxUser);
			}
		
		}
		catch (Exception e) {
			
		}
		finally {
			// redis 트렌잭션 종료
			releaseUserAddLock(userSn);
		}
		
		return okxUser;
	}
	
	
	public boolean getUserAddLock(long userSn) {
		String key = PGConstants.REDIS_USER_ADD_LOCK_PREFIX + Long.toString(userSn);
		boolean tf = redis.opsForValue().setIfAbsent(key, true);
		if (tf) {
			redis.expire(key, 60, TimeUnit.SECONDS);
			return true;
		} else {
			return false;
		}
	}
	

	public void releaseUserAddLock(long userSn) {
		redis.delete(PGConstants.REDIS_USER_ADD_LOCK_PREFIX + Long.toString(userSn));
	}

	
}
