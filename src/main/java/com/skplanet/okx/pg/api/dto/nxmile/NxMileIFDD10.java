package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD10 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0L(전체 Data건수(0L0L1로 셋팅해도 무방))")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램IDFALSE")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분FALSE")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분FALSE")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호FALSE")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0L:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678         ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가맹점사업자번호FALSE")
    private Long merchantBizNo = 0L;
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "주민번호(‘-‘제외)")
    private String ssn = "";
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "제휴사 가맹점명FALSE")
    private String partnerMerchantName = "";
    @NxmileColumn(size = 25, paddingBefore = false)
    @NxmileComment(value = "제휴사 가맹점번호FALSE")
    private String partnerMerchantNo = "";
    @NxmileColumn(size = 12, paddingBefore = false)
    @NxmileComment(value = "제휴사 승인번호FALSE")
    private String partnerApproveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "전표코드(코드값 참조)")
    private String slipCd = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "거래구분코드(코드값 참조)")
    private String trxTypeCd = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "복합결제여부(N)")
    private String complexPayYN = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래금액총합계(총 사용금액(OC+OCB))")
    private Long trxAmtTotSum = 0L;
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "계약구분코드1(코드값 참조)")
    private String contractTypeCd1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래금액1/요청포인트1(‘0L0L0L0L0L0L0L40L0L’)")
    private Long trxAmt1 = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "가맹점수수료1FALSE")
    private String merchantFee1 = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "계약구분코드2(코드값 참조)")
    private String contractTypeCd2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래금액2/요청포인트2(‘0L0L0L0L0L0L0L40L0L’)")
    private Long trxAmt2 = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "가맹점수수료2FALSE")
    private String merchantFee2 = "";
    @NxmileColumn(size = 16, paddingBefore = false, trim = false)
    @NxmileComment(value = "비밀번호FALSE")
    private String password = "";
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "현금거래금액(현금거래금액(공급가액+부가가치세+봉사료))")
    private Long cashTrxAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "공급가액(‘0L0L0L0L0L0L0L0L0L’)")
    private Long splyAmt = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "부가가치세(‘0L0L0L0L0L0L0L0L0L’)")
    private Long vat = 0L;
    @NxmileColumn(size = 9, value = "0")
    @NxmileComment(value = "봉사료(‘0L0L0L0L0L0L0L0L0L’)")
    private Long serviceAmt = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "용도구분(소비자소득공제용(0L:기본값)" +
            "사업자지출증빙용(1))")
    private Long usageType = 0L;
    @NxmileColumn(size = 1, value = "0")
    @NxmileComment(value = "현금영수증처리구분(적립//할인+현금(0L), 적립/할인Only(1))")
    private Long cashReceiptProcType = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "현금영수증취소사유(1:거래취소,2:오류취소,3:기타)")
    private String cashReceiptCancelRsn = "";
    @NxmileColumn(size = 3, value = "0", repeat = true)
    private Long gridCnt2 = 0L;
    @NxmileColumn(size = 30, doPadding = false)
    private List<Sub2> sub1 = new ArrayList<>();

    @Data
    public static class Sub2 {
        @NxmileColumn(size = 18, paddingBefore = false)
        @NxmileComment(value = "상품바코드(해당상품바코드)")
        private String productBarCd = "";
        @NxmileColumn(size = 5, value = "0")
        @NxmileComment(value = "상품수량(해당상품수량)")
        private Long productQty = 0L;
        @NxmileColumn(size = 6, paddingBefore = false)
        @NxmileComment(value = "제조사코드(납품업체코드)")
        private String makerCd = "";
        @NxmileColumn(size = 1, paddingBefore = false)
        @NxmileComment(value = "포인트구분FALSE")
        private String pointType = "";
    }

    public NxMileIFDD10(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD10 of() {
        return NxMileIFDD10.of(null);
    }

    public static NxMileIFDD10 of(String resCode) {
        return new NxMileIFDD10("DD10", resCode);
    }

    public NxMileIFDD10 defaultValue() {
        return setGridCnt0(1L)
                .setMembershipPgmId("A")
                .setMembershipSvcType("A1")
                .build();
    }
}
