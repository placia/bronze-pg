package com.skplanet.okx.pg.api.service.external.nxmile;

import com.google.common.collect.ImmutableMap;
import com.skplanet.okx.pg.api.dto.OCBPointCancelReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointCancelResDto;
import com.skplanet.okx.pg.api.dto.TxResType;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK410;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK411;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public abstract class AbstractNxMileCancelUseOCBPoint extends NxMileIFExecutor<OCBPointCancelReqDto, OCBPointCancelResDto> {
    public AbstractNxMileCancelUseOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public OCBPointCancelResDto execute(OCBPointCancelReqDto pointReqDto) {
        NxMileIFK410 request = getNxMileIFK410(pointReqDto);
        NxMileIFK411 response;

        try {
            response = nxMileConn.sendAndReceive(request, NxMileIFK411.class);
        } catch (IOException e) {
            log.error("Nxmile IOException", e);
            throw new NxMileException("NXMILE 송수신 실패", ImmutableMap.<String, Object> builder()
                    .putAll(request.valuesForException())
                    .build());
        }

        OCBPointCancelResDto pointResDto;

        try {
            failHandleService.test(request, response);
            pointResDto = getOcbPointCancelUseResForSuccess(response);
        } catch (NxMileException e) {
            log.error("OCB포인트 사용취소요청 실패:{}:({}:{})", request.getTrackingNo(), pointReqDto.getOrgApprovalNo(), response.getResultCode(), e);
            pointResDto = getOcbPointCancelUseResForFailure(response);
        }

        return pointResDto;
    }

    private OCBPointCancelResDto getOcbPointCancelUseResForFailure(NxMileIFK411 response) {
        OCBPointCancelResDto pointResDto;
        pointResDto = new OCBPointCancelResDto();
        pointResDto.setResultType(TxResType.FAIL);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg(response.getMessage1());
        pointResDto.setTrackingNo(response.getTrackingNo());

        return pointResDto;
    }

    private OCBPointCancelResDto getOcbPointCancelUseResForSuccess(NxMileIFK411 response) {
        OCBPointCancelResDto pointResDto;
        pointResDto = new OCBPointCancelResDto();
        pointResDto.setResultType(TxResType.SUCCESS);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg(response.getMessage1());
        pointResDto.setBalance(response.getUsablePoint1());
        pointResDto.setApprovalNo(response.getApproveNo());
        pointResDto.setApprovalDate(LocalDateTime.parse(response.getApproveDate()+response.getApproveTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        pointResDto.setTrackingNo(response.getTrackingNo());
        return pointResDto;
    }

    protected abstract NxMileIFK410 getNxMileIFK410(OCBPointCancelReqDto pointReqDto);
}
