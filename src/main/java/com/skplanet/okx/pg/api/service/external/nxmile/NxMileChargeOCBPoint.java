package com.skplanet.okx.pg.api.service.external.nxmile;

import com.skplanet.okx.pg.api.dto.OCBPointUseReqDto;
import com.skplanet.okx.pg.api.dto.OCBPointUseResDto;
import com.skplanet.okx.pg.api.dto.TxResType;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK100;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK101;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileResultCode;
import com.skplanet.okx.pg.api.service.NxMileFailHandleService;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import com.skplanet.okx.pg.support.exception.impl.NxMileNetCancelException;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class NxMileChargeOCBPoint extends NxMileIFExecutor<OCBPointUseReqDto, OCBPointUseResDto> {
    public NxMileChargeOCBPoint(NxMileFailHandleService failHandleService, NxMileConn nxMileConn) {
        super(failHandleService, nxMileConn);
    }

    @Override
    public OCBPointUseResDto execute(OCBPointUseReqDto pointReqDto) {
        NxMileIFK100 request = getNxMileIFK100(pointReqDto);
        NxMileIFK101 response = null;


        OCBPointUseResDto pointResDto;
        try {
//			재시도 포함 최대 2회 요청, 응답코드가 9999, 9070이거나 IOException 발생시 재시도
            response = nxMileConn.sendWithTryAndReceive(request, NxMileIFK101.class,
                    2,
                    req -> {},	//재전송시엔 응답코드가 '50'이지만, PP에서 주석으로 막혀있음.
                    resp -> NxMileResultCode.TIME_OVER.isEquals(resp.getResultCode()) ||
                            NxMileResultCode.ERROR_9070.isEquals(resp.getResultCode()));	// 응답값이 9999, 9070인 경우 1회 재전송
        } catch (NxMileException e) {
            log.error("OCB포인트 충전요청 오류:{}", request.getTrackingNo());
            throw e;
        }

        try {
//			아래 케이스인 경우 망취소 필요. NxMileNetCancelException throw
//				SYSTEM_ERROR1("9080", "시스템실연락바람")
//				SYSTEM_ERROR2("9966", "시스템실연락바람")
//				TIME_OVER("9999", "시간초과재시도요망")
//			아닌경우 NxMIleException throw

            failHandleService.test(request, response);

            pointResDto = getOcbPointChargeResForSuccess(pointReqDto, response);
        } catch (NxMileNetCancelException e) {
            log.error("망취소 요청처리 필요:{}", request.getTrackingNo(), e);

            //TODO: 이 메소드를 호출하는 서비스는 NETWORK_ERROR인 경우 망취소 처리가 필요하다.
            pointResDto = getOcbPointChargeResForFailure(response, TxResType.NETWORK_ERROR, "망취소");
        } catch (NxMileException e) {
            log.error("OCB포인트 사용요청 실패:{}:{}", request.getTrackingNo(), response.getResultCode(), e);
            pointResDto = getOcbPointChargeResForFailure(response, TxResType.FAIL, response.getMessage1());
        }

        return pointResDto;
    }

    private OCBPointUseResDto getOcbPointChargeResForSuccess(OCBPointUseReqDto pointReqDto, NxMileIFK101 response) {
        OCBPointUseResDto pointResDto = new OCBPointUseResDto();
        pointResDto.setResultType(TxResType.SUCCESS);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg("SUCCESS");
        pointResDto.setTrackingNo(response.getTrackingNo());
        pointResDto.setTxOcbPoint(pointReqDto.getTxOcbPoint());
        pointResDto.setBalance(response.getUsablePoint1());
        pointResDto.setApprovalNo(response.getApproveNo());
        pointResDto.setApprovalDate(LocalDateTime.parse(response.getApproveDate()+response.getApproveTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));

        return pointResDto;
    }

    private OCBPointUseResDto getOcbPointChargeResForFailure(NxMileIFK101 response, TxResType resultType, String resultMsg) {
        OCBPointUseResDto pointResDto = new OCBPointUseResDto();
        pointResDto.setResultType(resultType);
        pointResDto.setResultCode(response.getResultCode());
        pointResDto.setResultMsg(resultMsg);
        pointResDto.setTrackingNo(response.getTrackingNo());
        return pointResDto;
    }

    private NxMileIFK100 getNxMileIFK100(OCBPointUseReqDto pointReqDto) {
        return (NxMileIFK100) NxMileIFK100.of()
                .setMerchantNo(pointReqDto.getMctNo())
                .setMerchantBizNo(pointReqDto.getBizNo())
                .setTrxDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(pointReqDto.getTxDate()))
                .setTrxTime(DateTimeFormatter.ofPattern("HHmmss").format(pointReqDto.getTxDate()))
                .setTrackData(pointReqDto.getCrdNo())
                .setSlipCd("01")
                .setComplexPayYn("N")
                .setTrxTypeCd("22")
                .setContractTypeCd1("CD")
                .setTrxAmt1(pointReqDto.getTxOcbPoint())
                .setUsageType(0L)
                .setCashReceiptProcType(1L)
                .setWebSvcCallType("N")
                .setTrackingNo(pointReqDto.getTrackingNo());
    }
}
