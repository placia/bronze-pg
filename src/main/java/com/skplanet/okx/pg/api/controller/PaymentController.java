package com.skplanet.okx.pg.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skplanet.okx.pg.api.dto.payment.OKXPayReqDto;
import com.skplanet.okx.pg.api.dto.payment.OcbPointReq;
import com.skplanet.okx.pg.api.dto.payment.PayPrepareReq;
import com.skplanet.okx.pg.api.dto.payment.PayPrepareRes;
import com.skplanet.okx.pg.api.dto.payment.PayReqFromNxMile;
import com.skplanet.okx.pg.api.dto.payment.PayReqToNxMile;
import com.skplanet.okx.pg.api.dto.payment.PayResToNxMile;
import com.skplanet.okx.pg.api.dto.payment.PayExecuteReq;
import com.skplanet.okx.pg.api.dto.payment.PayExecuteRes;
import com.skplanet.okx.pg.api.dto.user.add.OCBUserAddResDto;
import com.skplanet.okx.pg.api.service.OCBService;
import com.skplanet.okx.pg.api.service.PaymentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "OCB 결제")
@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	OCBService ocbService;
	
	
	@ApiOperation("NxMile로 부터 합산 사용 요청 받기")
	@PostMapping(value="fromNxMile")
	public boolean fromNxMile(@Valid @RequestBody PayReqFromNxMile req) {
		return true;
	}
	
	@ApiOperation("NxMile에게 합산 사용 요청 하기")
	@PostMapping(value="toNxMile")
	public PayResToNxMile toNxMile(@Valid @RequestBody PayReqToNxMile req) {
		return ocbService.payToNxMile(req);
	}
	
	@ApiOperation("ocb포인트 조회")
	@PostMapping(value="getOcbPoint")
	public long getOcbPoint(@Valid @RequestBody OcbPointReq req) {
		return ocbService.getOcbPoint(req);
	}
	
	@ApiOperation("자금 거래 준비, 단계 1 - 요청 유효성 확인 및 잔액 확인절차")
	@PostMapping(value="prepare")
	public PayPrepareRes payPrepare(@Valid @RequestBody PayPrepareReq req) {
		PayPrepareRes res = paymentService.payPrepare(req);
		return res;
	}

	@ApiOperation("자금 거래 실행, 단계 2 - 블록체인에 실제 거래처리 체결")
	@PostMapping(value="execute")
	public PayExecuteRes payExecute(@Valid @RequestBody PayExecuteReq req) {
		PayExecuteRes res = paymentService.payExecute(req);
		return res;
	}
	
	
	@ApiOperation("취소")
	@PostMapping(value="cancel")
	public OCBUserAddResDto cancel(@RequestBody OKXPayReqDto prd) {
		OCBUserAddResDto res = new OCBUserAddResDto();
		return res;
	}
	
}
