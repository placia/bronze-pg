package com.skplanet.okx.pg.api.dto.payment;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "NxMile로부터 합산 사용 요청의 입력 파라미터")
@Data
public class PayReqFromNxMile {
	
	@NotNull
	@ApiModelProperty(value = "okx용 카드번호 16자리", required = true, example = "1111222233334444")
	private String crdNo;
	
	@NotNull
	@ApiModelProperty(value = "합산사용으로 차감할 금액", required = true, example = "1000")
	private long amount;
}
