package com.skplanet.okx.pg.api.dto.user.add;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "회원 추가 응답 파라미터")
@Data
public class OCBUserAddResDto {
	@ApiModelProperty(value = "OKX 회원번호", required = true, example = "1111222233334444")
	private long userSn;
	
	@ApiModelProperty(value = "가입/OCB연동 상태, 0-정상, 1-중지, 2-OCB카드미등록(탈퇴중 등의 사유로 재가입이 안됨), 3-OCB합산사용안됨", required = true)
	private String status;
	
}
