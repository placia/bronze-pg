package com.skplanet.okx.pg.api.controller;

import com.skplanet.ocb.tcp.annotation.TcpController;
import com.skplanet.ocb.tcp.annotation.TcpMapping;
import com.skplanet.okx.pg.api.dto.nxmile.*;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Slf4j
@TcpController
public class NxMileController {
    /**
     * OKX 사용요청
     * @param request
     * @return
     */
    @TcpMapping("DD10")
    public NxMileIFDD11 pay(NxMileIFDD10 request) {
        //blockchain confirm datetime
        LocalDateTime blockCreateDateTime = LocalDateTime.now();
        //blockchain txhash와 매핑된 외부 발행용 거래고유번호 취소에서 NxMile이 사용함(9자리)
        String nxmileRespAprvNo = String.valueOf(new Random().nextInt(1000000000));

        //OKX 최종잔액
        long balance = 100000L;

        return (NxMileIFDD11) NxMileIFDD11.of(NxMileResultCode.SUCCESS.value())
                .setMerchantNo(request.getMerchantNo())
                .setTrxDate(request.getTrxDate())
                .setTrxTime(request.getTrxTime())
                .setTrackData(request.getTrackData())
                .setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(blockCreateDateTime))
                .setApproveTime(DateTimeFormatter.ofPattern("hhmmss").format(blockCreateDateTime))
                .setApproveNo(nxmileRespAprvNo)
                .setDiscRealSubstractPoint(request.getTrxAmt1())
                .setUsablePoint1(balance)
                .setAccumPoint1(balance)
                .setSendDate(request.getSendDate())
                .setSendTime(request.getSendTime())
                .setTrackingNo(request.getTrackingNo());
    }

    /**
     * OKX 사용취소/사용망취소
     * @param request
     * @return
     */
    @TcpMapping("DD20")
    public NxMileIFDD21 cancel(NxMileIFDD20 request) {
        if ("60".equals(request.getResCdLcls())) {
            log.info("망취소요청: [{}] OCB사용승인번호:{}, OCB사용요청일자:{}"
                    , request.getTrackingNo()          //PP10 승인 요청 trackingNo와 동일
                    , request.getPartnerApproveNo()
                    , request.getOrgTrxDate()
            );
        } else {
            log.info("사용취소요청: [{}] OCB사용취소승인번호:{}, OCB원거래일자:{}, 원거래TxId:{}, OCB원거래승인번호:{}"
                    , request.getTrackingNo()
                    , request.getPartnerApproveNo()
                    , request.getOrgTrxDate()
                    , request.getOrgApproveNo()
                    , request.getOrgTrxPartnerApproveNo()
            );
        }

        //blockchain confirm datetime
        LocalDateTime blockCreateDateTime = LocalDateTime.now();
        //blockchain txhash와 매핑된 외부 발행용 거래고유번호 취소에서 NxMile이 사용함(9자리)
        String nxmileRespAprvNo = String.valueOf(new Random().nextInt(1000000000));
        //PP10 사용거래금액
        long orgAmt = 300000L;
        //OKX 최종잔액
        long balance = 100000L;

        return (NxMileIFDD21) NxMileIFDD21.of(NxMileResultCode.SUCCESS.value())
                .setMerchantNo(request.getMerchantNo())
                .setTrxDate(request.getTrxDate())
                .setTrxTime(request.getTrxTime())
                .setTrackData(request.getTrackData())
                .setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(blockCreateDateTime))
                .setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(blockCreateDateTime))
                .setApproveNo(nxmileRespAprvNo)
                .setUsablePoint1(balance)
                .setAccumPoint1(balance)
                .setRealSubstractPoint1(orgAmt)
                .setSendTime(request.getSendTime())
                .setTrackingNo(request.getTrackingNo());
    }

    /**
     * OKX 잔액조회
     * @param request
     * @return
     */
    @TcpMapping("DD30")
    public NxMileIFDD31 balance(NxMileIFDD30 request) {
        //OKX 잔액
        long balance = 100000L;

        return NxMileIFDD31.of(NxMileResultCode.SUCCESS.value())
                .setMerchantNo(request.getMerchantNo())
                .setTrackData(request.getTrackData())
                .setRequestType("RD")
                .setIsUsableType("Y")
                .setUsablePoint1(balance)
                .setAccumPoint1(balance)
                ;
    }

    /**
     * OKX 사용망상재사용
     * @param request
     * @return
     */
    @TcpMapping("DD40")
    public NxMileIFDD41 rePayForNwError(NxMileIFDD40 request) {
        //blockchain confirm datetime
        LocalDateTime blockCreateDateTime = LocalDateTime.now();
        //blockchain txhash와 매핑된 외부 발행용 거래고유번호 취소에서 NxMile이 사용함(9자리)
        String nxmileRespAprvNo = String.valueOf(new Random().nextInt(1000000000));
        //OKX 최종잔액
        long balance = 100000L;

        return (NxMileIFDD41) NxMileIFDD41.of(NxMileResultCode.SUCCESS.value())
                .setMerchantNo(request.getMerchantNo())
                .setTrxDate(request.getTrxDate())
                .setTrxTime(request.getTrxTime())
                .setTrackData(request.getTrackData())
                .setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(blockCreateDateTime))
                .setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(blockCreateDateTime))
                .setApproveNo(nxmileRespAprvNo)
                .setUsablePoint1(balance)
                .setAccumPoint1(balance)
                .setTrackingNo(request.getTrackingNo());
    }
}
