package com.skplanet.okx.pg.api.dto.nxmile;

import com.skplanet.ocb.tcp.annotation.NxmileColumn;
import com.skplanet.ocb.tcp.annotation.NxmileComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class NxMileIFDD11 extends NxMileIFHeader {
    @NxmileColumn(size = 3, value = "0")
    @NxmileComment(value = "GRID_CNT0(전체 Data건수)")
    private Long gridCnt0 = 0L;
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "멤버쉽프로그램IDFALSE")
    private String membershipPgmId = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "멤버쉽서비스구분FALSE")
    private String membershipSvcType = "";
    @NxmileColumn(size = 2, paddingBefore = false)
    @NxmileComment(value = "단말기구분FALSE")
    private String terminalType = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "단말기번호FALSE")
    private String terminalNo = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "WCC(0:MSR, 1:IC(MS), 2:Key In, 3:IC(Touch))")
    private String wcc = "";
    @NxmileColumn(size = 15, paddingBefore = false)
    @NxmileComment(value = "가맹점번호(‘12345678       ‘)")
    private String merchantNo = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "거래일자(YYYYMMDD)")
    private String trxDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "거래시간(hhmmss)")
    private String trxTime = "";
    @NxmileColumn(size = 37, paddingBefore = false)
    @NxmileComment(value = "Track II Data(카드번호)")
    private String trackData = "";
    @NxmileColumn(size = 13, paddingBefore = false)
    @NxmileComment(value = "주민번호FALSE")
    private String ssn = "";
    @NxmileColumn(size = 8, paddingBefore = false)
    @NxmileComment(value = "승인일자(YYYYMMDD)")
    private String approveDate = "";
    @NxmileColumn(size = 6, paddingBefore = false)
    @NxmileComment(value = "승인시간(hhmmss)")
    private String approveTime = "";
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "승인번호FALSE")
    private String approveNo = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 1(통합승인사용)")
    private String message1 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 2(통합승인사용)")
    private String message2 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 3(대외기관사용)")
    private String message3 = "";
    @NxmileColumn(size = 64, paddingBefore = false)
    @NxmileComment(value = "Message 4(대외기관사용)")
    private String message4 = "";
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명1FALSE")
    private String pointTypeName1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "할인금액FALSE")
    private Long discAmt = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "할인실차감포인트FALSE")
    private Long discRealSubstractPoint = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "할인후잔여금액FALSE")
    private Long discAfterRemainAmt = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "할인후잔액 적립포인트FALSE")
    private Long discAfterRemainAmtAccumPoint = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래발생포인트1FALSE")
    private Long trxOccrPoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "상품발생포인트1FALSE")
    private Long productOccrPoint1 = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "가맹점수수료1FALSE")
    private String merchantFee1 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트1(사용 가능한 포인트)")
    private Long usablePoint1 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트1(현재포인트 + 가용포인트)")
    private Long accumPoint1 = 0L;
    @NxmileColumn(size = 10, paddingBefore = false)
    @NxmileComment(value = "포인트구분명2((전환시 전환TRG포인트))")
    private String pointTypeName2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "거래발생포인트2FALSE")
    private Long trxOccrPoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "상품발생포인트2FALSE")
    private Long productOccrPoint2 = 0L;
    @NxmileColumn(size = 9, paddingBefore = false)
    @NxmileComment(value = "가맹점수수료2FALSE")
    private String merchantFee2 = "";
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "가용포인트2(사용가능한 포인트)")
    private Long usablePoint2 = 0L;
    @NxmileColumn(size = 10, value = "0")
    @NxmileComment(value = "누적포인트2(현재포인트 + 가용포인트)")
    private Long accumPoint2 = 0L;
    @NxmileColumn(size = 2, value = "0")
    @NxmileComment(value = "현금 처리여부(→현금처리 안된 이유 코드집 활용)")
    private Long cashProcType = 0L;
    @NxmileColumn(size = 40, paddingBefore = false)
    @NxmileComment(value = "현금 오류메시지(현금영수증 오류 관련 메시지" +
            "(오류시 : 현금응답코드 소분류(2) + Space(1) + 오류메세지 내용))")
    private String cashErrorMessage = "";
    @NxmileColumn(size = 1, paddingBefore = false)
    @NxmileComment(value = "현금영수증취소사유(1:거래취소,2:오류취소,3:기타)")
    private String cashReceiptCancelRsn = "";
    @NxmileColumn(size = 3, value = "0", repeat = true)
    private Long gridCnt2 = 0L;
    @NxmileColumn(size = 63, doPadding = false)
    private List<Sub2> sub1 = new ArrayList<>();

    @Data
    public static class Sub2 {
        @NxmileColumn(size = 18, paddingBefore = false)
        @NxmileComment(value = "상품바코드(해당상품코드)")
        private String productBarCd = "";
        @NxmileColumn(size = 10, paddingBefore = false)
        @NxmileComment(value = "포인트구분명1FALSE")
        private String pointTypeName1 = "";
        @NxmileColumn(size = 10, value = "0")
        @NxmileComment(value = "적립포인트1FALSE")
        private Long accumPoint1 = 0L;
        @NxmileColumn(size = 10, paddingBefore = false)
        @NxmileComment(value = "포인트구분명2FALSE")
        private String pointTYpeName2 = "";
        @NxmileColumn(size = 10, value = "0")
        @NxmileComment(value = "적립포인트2FALSE")
        private Long accumPoint2 = 0L;
        @NxmileColumn(size = 5, value = "0")
        @NxmileComment(value = "적립상품개수(해당상품개수)")
        private Long accumProductCnt = 0L;
    }

    public NxMileIFDD11(String msgGrpCd, String resCode) {
        super(msgGrpCd, resCode);
        this.defaultValue();
    }

    public static NxMileIFDD11 of() {
        return NxMileIFDD11.of(null);
    }

    public static NxMileIFDD11 of(String resCode) {
        return new NxMileIFDD11("DD11", resCode);
    }

    public NxMileIFDD11 defaultValue() {
        return setGridCnt0(1L)
                .build();
    }
}
