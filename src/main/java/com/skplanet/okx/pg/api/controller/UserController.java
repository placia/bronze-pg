package com.skplanet.okx.pg.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skplanet.okx.pg.api.dto.user.add.OCBUserAddResDto;
import com.skplanet.okx.pg.api.dto.user.add.OKXUserAddReqDto;
import com.skplanet.okx.pg.api.service.OCBService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "회원 관리")
@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private OCBService ocbService;
	
	
	@ApiOperation("사용자 가입")
	@PostMapping(value="add")
	public OCBUserAddResDto addMember(@Valid @RequestBody OKXUserAddReqDto uard) {
		return ocbService.addUser(uard);
	}
	
	
	@ApiOperation("조회(1건)")
	@GetMapping(value="get")
	public ResponseEntity<String> get() {
		return new ResponseEntity<>("ok", HttpStatus.OK);
	}
}
