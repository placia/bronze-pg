package com.skplanet.okx.pg.api.controller;

import com.skplanet.ocb.tcp.annotation.TcpController;
import com.skplanet.ocb.tcp.annotation.TcpMapping;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFW400;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@TcpController
@RestController
@RequestMapping(value = "/")
public class MainController {

	
	@GetMapping(value="/hello")
	public ResponseEntity<String> main() {
		return new ResponseEntity<>("main", HttpStatus.OK);
	}
}
