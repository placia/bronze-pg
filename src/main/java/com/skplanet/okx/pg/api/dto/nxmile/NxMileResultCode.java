package com.skplanet.okx.pg.api.dto.nxmile;

import lombok.Getter;

import java.util.Arrays;

public enum NxMileResultCode {
    SUCCESS("0000", "SUCCESS")
    , INVALID_USER("4429", "회원상태비정상")
    , NO_APPR_NO("7746", "원거래승인번호미전송")      //
    , ERROR_9070("9070", "NXMILE 오류")
    , SYSTEM_ERROR1("9080", "시스템실연락바람")
    , SYSTEM_ERROR2("9966", "시스템실연락바람")
    , TIME_OVER("9999", "시간초과재시도요망")
    , DEFAULT("", "NXMILE 오류")
    ;

    private String code;
    @Getter
    private String message;

    NxMileResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public boolean isEquals(String code) {
        return this.code.equals(code);
    }

    public String value() {
        return this.code;
    }

    public static NxMileResultCode findErrorCode(final String resultCode) {
        return Arrays.stream(NxMileResultCode.values())
                .filter(value -> value.isEquals(resultCode))
                .findFirst()
                .orElse(NxMileResultCode.DEFAULT);
    }
}
