package com.skplanet.okx.pg.config;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@EnableCaching
//@Configuration
public class CacheConfig implements CachingConfigurer {
	
    @Bean 
    @Override 
    public CacheErrorHandler errorHandler() { 
        return new CacheErrorHandler() { 
 
            @Override 
            public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) { 
                log.warn(cache.getName(), exception); 
            } 
 
            @Override 
            public void handleCachePutError(RuntimeException exception, Cache cache, Object key, Object value) { 
            	log.warn(cache.getName(), exception); 
            } 
 
            @Override 
            public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) { 
            	log.warn(cache.getName(), exception); 
            } 
 
            @Override 
            public void handleCacheClearError(RuntimeException exception, Cache cache) { 
            	log.warn(cache.getName(), exception); 
            } 
        }; 
    }

	@Override
	public CacheManager cacheManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CacheResolver cacheResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KeyGenerator keyGenerator() {
		// TODO Auto-generated method stub
		return null;
	}


}
