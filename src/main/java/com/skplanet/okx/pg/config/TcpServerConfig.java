package com.skplanet.okx.pg.config;

import com.skplanet.ocb.tcp.TcpControllerRegistry;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TcpServerConfig {
    @Autowired
    private BeanFactory beanFactory;

    @Value("${tcp.charset}")
    private String tcpCharset;

    @Bean
    public TcpControllerRegistry controllerRegistry() {
        return new TcpControllerRegistry(beanFactory).load("com.skplanet.okx.pg");
    }
}
