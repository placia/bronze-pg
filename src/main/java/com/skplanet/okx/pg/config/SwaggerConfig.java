package com.skplanet.okx.pg.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;
import com.skplanet.okx.pg.constant.PGConstants;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public ApiInfo apiInfo() {
		
		return new ApiInfoBuilder()
			.title("Payment Gateway API")
			.description("PG API Documentation")
			.version("v1")
			.build()
		;
	}
		
	@Bean
	public Docket docket(ApiInfo apiInfo) {
		List<springfox.documentation.service.Parameter> gParams = new ArrayList<>();
		gParams.add(new ParameterBuilder()
				.name(PGConstants.PG_API_HEADER_TOKEN_NAME)
				.description("basic auth header")
				.modelRef(new ModelRef("string"))
				.parameterType("header")
				.required(true)
				.build());
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(Predicates.not(PathSelectors.regex("/error")))
				.build()
				.apiInfo(apiInfo)
				.globalOperationParameters(gParams);
		
	}
}