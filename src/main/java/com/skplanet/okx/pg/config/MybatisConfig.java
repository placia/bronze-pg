package com.skplanet.okx.pg.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisConfig {

	@Autowired
	private DataSource dataSource;
	
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
		config.setJdbcTypeForNull(JdbcType.NULL);
		config.setCallSettersOnNulls(true);

		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setConfiguration(config);
		bean.setTypeHandlersPackage("com.skplanet.okx.pg.mybatis.typehandler");
		
		return bean.getObject();
	}
	
}
