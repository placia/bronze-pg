package com.skplanet.okx.pg.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

	@Autowired
	RedisConnectionFactory connectionFactory;
	
	@Bean
	public RedisTemplate<String, Object> geObjectRedisTemplate() {
		final RedisTemplate<String, Object>template = new RedisTemplate<>();
		RedisSerializer<String> stringSerializer = new StringRedisSerializer();
		template.setConnectionFactory(connectionFactory);
		template.setKeySerializer(stringSerializer);
		template.setHashKeySerializer(stringSerializer);
		template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
		
		return template;
	}
	
	@Bean
	public StringRedisTemplate getStringRedisTemplate() {
		return new StringRedisTemplate(connectionFactory);
	}
}
