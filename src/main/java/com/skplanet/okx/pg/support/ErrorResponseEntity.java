package com.skplanet.okx.pg.support;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

public class ErrorResponseEntity extends ResponseEntity<ErrorResponse> {

  public ErrorResponseEntity(ErrorResponse body) {
    super(body, body.getStatus());
  }

  public ErrorResponseEntity(ErrorResponse body, MultiValueMap<String, String> headers) {
    super(body, headers, body.getStatus());
  }
  
  
  
  public static ErrorResponseEntity badReqeust(String message) {
    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST, message));
  }
  
  public static ErrorResponseEntity badReqeust(String message, Map<String, Object> bindingResult) {
	    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST, message, bindingResult));
	  }

  public static ErrorResponseEntity unAuthorized(String message) {
    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.UNAUTHORIZED, message));
  }

  public static ErrorResponseEntity forbbiden(String message, Map<String, Object> bindingResult) {
    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.FORBIDDEN, message, bindingResult));
  }

  public static ErrorResponseEntity notFound(String message) {
    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, message));
  }
  
  public static ErrorResponseEntity notAcceptable(String message) {
	    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.NOT_ACCEPTABLE, message));
  }  
  
  public static ErrorResponseEntity serverError(String message) {
    return new ErrorResponseEntity(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, message));
  }

}
