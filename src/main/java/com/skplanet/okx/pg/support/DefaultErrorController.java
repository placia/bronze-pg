package com.skplanet.okx.pg.support;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.skplanet.okx.pg.constant.PGConstants;



@RestController
public class DefaultErrorController implements ErrorController {

  private static final String ERROR_PATH = "${error.path:/error}";
  private static final String ERROR_DEFAULT_MESSAGE = "No message available.";
  
  private String errorPath;
  private ErrorAttributes errorAttributes;
  private MessageSource messageSource;

	@Autowired
	Environment environment;
	
  
  
  @RequestMapping(value=ERROR_PATH)
  public ResponseEntity<Map<String, Object>> error(HttpServletRequest request, Locale locale) {
    ServletWebRequest servletWebRequest = new ServletWebRequest(request);
    Map<String, Object> body = errorAttributes.getErrorAttributes(servletWebRequest, true);

    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    if (Objects.nonNull(body.get("status"))) {
      try {
        status = HttpStatus.valueOf((Integer) body.get("status"));
      } catch (Exception ignore) {
      }
    }

    

	if (!environment.acceptsProfiles(PGConstants.PROFILE_LOCAL)) {
		if (body.containsKey("trace")) body.remove("trace");
		if (body.containsKey("exception")) body.remove("exception");
		if (body.containsKey("errors")) body.remove("errors");
		
		if (body.containsKey("message")) {
			String errorMessage = (String) body.get("message");
			
			try {
				int idx = errorMessage.indexOf('(');
				errorMessage = errorMessage.substring(0, idx - 1);
			}
			catch (Exception e) {
				errorMessage = "invalid argument data!";
			}
		}
			
	}
	else {

		if (!body.containsKey("message")) {
		    String message = messageSource.getMessage("error." + status, null, ERROR_DEFAULT_MESSAGE, locale);
		    if (StringUtils.hasText(message)) {
		      body.put("message", message);
		    }
		}
		
	}
    
    return ResponseEntity.status(status).body(body);
  }

  @Override
  public String getErrorPath() {
    return errorPath;
  }

  @Value(ERROR_PATH)
  public void setErrorPath(String errorPath) {
    this.errorPath = errorPath;
  }

  @Autowired
  public void setErrorAttributes(ErrorAttributes errorAttributes) {
    this.errorAttributes = errorAttributes;
  }

  @Autowired
  public void setMessageSource(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

}