package com.skplanet.okx.pg.support.exception.impl;

import java.util.HashMap;
import java.util.Map;

import com.skplanet.okx.pg.support.exception.BadRequestException;



public class UserNotFoundException extends BadRequestException {
	private static final long serialVersionUID = -5197797544093445581L;
	private static final String userIdKeyName = "userId";
	private static final String errorMessage = "사용자를 찾을 수 없습니다";
	
	private static Map<String, Object> genBindingResult(String userId) {
		Map<String, Object> map = new HashMap<>();
		map.put(userIdKeyName, userId);
		return map;
	}
	
	public UserNotFoundException (String userId) {
		super(errorMessage, genBindingResult(userId));
	}

	
	public String getUserId() {
		return (String) this.getBindingResult().getOrDefault(userIdKeyName, null);
	}
}
