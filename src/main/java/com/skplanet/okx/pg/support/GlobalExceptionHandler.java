package com.skplanet.okx.pg.support;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.skplanet.okx.pg.support.exception.BadRequestException;
import com.skplanet.okx.pg.support.exception.ForbbidenException;
import com.skplanet.okx.pg.support.exception.NotFoundException;
import com.skplanet.okx.pg.support.exception.impl.GeneralBusinessException;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

	private static final String invalidParamMessage = "잘못된 요청 파라미터!";
	

	//
	// 아래는 내가 정한 오류 목록, 400 403 404
	//
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> badRequestException(BadRequestException exception, Locale locale) {
		String errorMessage = exception.getMessage();
		log.info(errorMessage);
		return ErrorResponseEntity.badReqeust(errorMessage, exception.getBindingResult());
	}
	
	@ExceptionHandler(ForbbidenException.class)
	public ResponseEntity<?> forbbidenException(ForbbidenException exception, Locale locale) {
		String errorMessage = exception.getMessage();
		log.info(errorMessage);
		return ErrorResponseEntity.forbbiden(errorMessage, exception.getBindingResult());
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> notFoundException(NotFoundException exception, Locale locale) {
		String errorMessage = exception.getMessage();
		log.info("{} {}", errorMessage, exception.getUrl());
		return ErrorResponseEntity.notFound(errorMessage);
	}

	
	
	//
	// 아래는 기본 오류 이외의 비즈니스 로직상, 스프링 혹은 시스템에서 날 수 있는 예외들 -> 400 500 으로 대체함
	//
	@ExceptionHandler(GeneralBusinessException.class)
	public ResponseEntity<?> genaralBusinessException(GeneralBusinessException exception, Locale locale) {
		String errorMessage = exception.getMessage();
		log.warn(errorMessage, exception);
		return ErrorResponseEntity.badReqeust(errorMessage, exception.getBindingResult());
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> badRequestException(HttpMessageNotReadableException exception, Locale locale) {

		log.warn(exception.getMessage());
		
		String errorMessage = exception.getMessage();
		
		try {
			int idx = errorMessage.indexOf('(');
			errorMessage = errorMessage.substring(0, idx - 1);
		}
		catch (Exception e) {
			errorMessage = "invalid argument data!";
		}
		
		String mapKey = "cause";
		
		Map<String, Object> map = new HashMap<>();
		map.put(mapKey, errorMessage);
		
		return ErrorResponseEntity.badReqeust(invalidParamMessage, map);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> badRequestException(MethodArgumentNotValidException exception, Locale locale) {

		log.warn(exception.getMessage());

		String errorMessage = "invalid argument format!";
		BindingResult br = exception.getBindingResult();
		if (br != null) {
			List<ObjectError> errors = br.getAllErrors();
			if (!CollectionUtils.isEmpty(errors) ) {
				errorMessage = errors.get(errors.size() - 1).getDefaultMessage();
			}
		}

		String mapKey = "cause";
		
		Map<String, Object> map = new HashMap<>();
		map.put(mapKey, errorMessage);
		
		return ErrorResponseEntity.badReqeust(invalidParamMessage, map);
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<?> httpRequestMethodNotSupportedException(HttpServletRequest request, ClientAbortException exception, Locale locale) {
		String errorMessage = exception.getMessage();
		log.warn("{} {} {}", request.getRequestURI(), errorMessage, exception);
		return ErrorResponseEntity.badReqeust(errorMessage);
	}

}

