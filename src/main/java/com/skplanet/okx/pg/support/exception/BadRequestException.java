package com.skplanet.okx.pg.support.exception;

import java.util.Map;

import org.springframework.context.MessageSourceResolvable;

/**
 * 서비스 전반적인 모든 비즈니스 오류들이 여기에 해당. 상세한 오류 정의는 impl 에 구현
 * @author 1001083
 *
 */
public class BadRequestException extends RuntimeException implements MessageSourceResolvable {

	private static final long serialVersionUID = 2685273987441646467L;

	private Map<String, Object> bindingResult;
	
	public BadRequestException(String message) {
		super(message);
	}
	
	public BadRequestException(String message, Map<String, Object> bindingResult) {
		super(message);
		this.bindingResult = bindingResult;
	}
	
	
	public BadRequestException(Throwable t) {
		super(t);
	}
	
	public BadRequestException(String m, Throwable t) {
		super(m, t);
	}

	@Override
	public Object[] getArguments() {
		return null;
	}

	@Override
	public String getDefaultMessage() {
		return getMessage();
	}

	@Override
	public String[] getCodes() {
		return null;
	}
	
	public Map<String, Object> getBindingResult() {
		return bindingResult;
	}
	
	public Object getBindingFieldByKey(String key) {
		return (bindingResult != null) ? bindingResult.get(key) : null;
	}
}