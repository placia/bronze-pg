package com.skplanet.okx.pg.support.exception.impl;

import com.skplanet.okx.pg.support.exception.NotFoundException;

public class GeneralResourceNotFoundException extends NotFoundException {

	private static final long serialVersionUID = 3464699282654987567L;

	public GeneralResourceNotFoundException(String msg, String url) {
		super(msg, url);
	}

	@Override
	public String[] getCodes() {
		return null;
	}

}
