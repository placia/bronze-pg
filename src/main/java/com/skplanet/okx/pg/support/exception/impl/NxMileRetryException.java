package com.skplanet.okx.pg.support.exception.impl;

public class NxMileRetryException extends RuntimeException {
	public NxMileRetryException() {
		super();
	}

	public NxMileRetryException(String message) {
		super(message);
	}
}
