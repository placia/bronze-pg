package com.skplanet.okx.pg.support.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.MessageSourceResolvable;

/**
 * API 및 기능에 대한 사용 권한 부족 오류 
 * @author 1001083
 *
 */
public class ForbbidenException extends RuntimeException implements MessageSourceResolvable {

	private static final long serialVersionUID = 7248585510178293038L;
	public static final String mapMainKey = "cause";
	
	public static enum ForbbidenType {Permit, Penalty, Block};
	
	private Map<String, Object> bindingResult;
	
	public ForbbidenException(String message, Map<String, Object> map) {
		super(message);
		bindingResult = map;
	}
  
	@Override
	public Object[] getArguments() {
		return null;
	}  

	@Override
	public String getDefaultMessage() {
		return getMessage();
	}

	@Override
	public String[] getCodes() {
		return null;
	}

	public Map<String, Object> getBindingResult() {
		return bindingResult;
	}
	
	
	public static class Builder {
		
		private ForbbidenType ft;
		private Map<String, Object> map;
		
		public Builder(ForbbidenType ft) {
			map = new HashMap<>();
			this.ft = ft;
			map.put(mapMainKey, ft.toString());
		}
		
		public Builder put(String key, Object val) {
			map.put(key, val);
			return this;
		}
		
		public ForbbidenException build() {
			return new ForbbidenException(ft.toString(), map);
		}
		
	}
}