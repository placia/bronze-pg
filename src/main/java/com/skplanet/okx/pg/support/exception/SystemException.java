package com.skplanet.okx.pg.support.exception;

import java.util.Map;

import org.springframework.context.MessageSourceResolvable;

/**
 * 로직 오류가 아닌 시스템/하드웨어 (메모리, 디비, 및 IO 관련) 장애 일 경우 
 * @author 1001083
 *
 */
public class SystemException extends RuntimeException implements MessageSourceResolvable {

	private static final long serialVersionUID = 5422688075422152915L;

	private Map<String, Object> bindingResult;
	
	public SystemException(String message) {
		super(message);
	}
	
	public SystemException(String message, Map<String, Object> bindingResult) {
		super(message);
		this.bindingResult = bindingResult;
	}
	
	
	public SystemException(Throwable t) {
		super(t);
	}
	
	public SystemException(String m, Throwable t) {
		super(m, t);
	}

	@Override
	public Object[] getArguments() {
		return null;
	}

	@Override
	public String getDefaultMessage() {
		return getMessage();
	}

	@Override
	public String[] getCodes() {
		return null;
	}
	
	public Map<String, Object> getBindingResult() {
		return bindingResult;
	}
}