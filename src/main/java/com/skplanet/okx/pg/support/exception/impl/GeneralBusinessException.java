package com.skplanet.okx.pg.support.exception.impl;

import com.skplanet.okx.pg.support.exception.BadRequestException;

public class GeneralBusinessException extends BadRequestException {

	private static final long serialVersionUID = 2871459126042078617L;

	public GeneralBusinessException(String msg) {
		super(msg);
	}
	
	public GeneralBusinessException(Throwable t) {
		super(t);
	}
	
	public GeneralBusinessException(String m, Throwable t) {
		super(m, t);
	}

}
