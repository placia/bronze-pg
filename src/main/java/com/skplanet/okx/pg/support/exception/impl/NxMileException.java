package com.skplanet.okx.pg.support.exception.impl;

import java.util.Map;

import com.skplanet.okx.pg.support.exception.BadRequestException;


public class NxMileException extends BadRequestException {

	private static final long serialVersionUID = 2871459126042074617L;
	
	public NxMileException(String message, Map<String, Object> bindingResult) {
		super(message, bindingResult);
		// TODO Auto-generated constructor stub
	}


}
