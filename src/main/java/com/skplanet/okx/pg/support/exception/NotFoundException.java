package com.skplanet.okx.pg.support.exception;

import org.springframework.context.MessageSourceResolvable;

/**
 * 경로나 파일 등이 존재 하지 않는 경우에 사용되는 오류
 * @author 1001083
 *
 */
public abstract class NotFoundException extends RuntimeException implements MessageSourceResolvable {

	private static final long serialVersionUID = -6269031545269157614L;

	private String url;
	
	public NotFoundException(String message, String url) {
		super(message);
		this.url = url;
	}
  
	public String getUrl() {
		return url;
	}
	
	@Override
	public Object[] getArguments() {
		return null;
	}  

	@Override
	public String getDefaultMessage() {
		return getMessage();
	}

}