package com.skplanet.okx.pg.support.exception.impl;

import com.skplanet.okx.pg.support.exception.BadRequestException;

public class InvalidParamException extends BadRequestException {

	private static final long serialVersionUID = -6373805686277419525L;

	public InvalidParamException(String msg) {
		super(msg);
	}

}
