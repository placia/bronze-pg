package com.skplanet.okx.pg.api.service;

import com.skplanet.ocb.tcp.domain.nxmile.NxmileMessageConverter;
import com.skplanet.okx.pg.api.dto.*;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK101;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK111;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK401;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFK411;
import com.skplanet.okx.pg.api.service.external.nxmile.NxMileConn;
import com.skplanet.okx.pg.api.service.mock.NxMileMockServer;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NxMileIFServiceTest {
    NxMileIFService ifService;
    ExecutorService executor;
    NxMileMockServer server;
    static AtomicInteger port = new AtomicInteger(10001);

    @Before
    public void setUp() throws Exception {
        ifService = new NxMileIFService();
        ifService.setNxMileConn(new NxMileConn()
                .setHost("localhost")
                .setPort(port.get())
                .setCharset("euc-kr")
        );
        ifService.setPredicatesService(new NxMileFailHandleService());

        executor = Executors.newFixedThreadPool(10);
        server = new NxMileMockServer();
        server.setPort(port.getAndIncrement());

        executor.submit(() -> {
            server.start();
        });

        TimeUnit.MILLISECONDS.sleep(500);
    }

    @Test
    public void validateOcbUser_정상() {
        server.setMockField(new NxMileMockServer.MockField("0000", 38, 4, "결과정상"),
                            new NxMileMockServer.MockField("W401", 0, 4, "결과정상"));

        ifService.validateOcbUser("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
    }

    @Test(expected = NxMileException.class)
    public void validateOcbUser_회원상태비정상() throws InterruptedException {
        server.setMockField(new NxMileMockServer.MockField("4429", 38, 4, "회원상태비정상"),
                            new NxMileMockServer.MockField("W401", 0, 4, "결과정상"));

        try {
            ifService.validateOcbUser("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void validateOcbUser_탈회상태() throws InterruptedException {
        server.setMockField(new NxMileMockServer.MockField("0000", 38, 4, "결과정상"),
                            new NxMileMockServer.MockField("N", 231, 1, "적립안함"),
                            new NxMileMockServer.MockField("W401", 0, 4, "결과정상"));

        try {
            ifService.validateOcbUser("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void addCard_정상() {
        server.setMockField(new NxMileMockServer.MockField("0000", 38, 4, "결과정상"),
                            new NxMileMockServer.MockField("M481", 0, 4, "결과정상"));

        ifService.addCard("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678", "1234567890123456");
    }

    @Test(expected = NxMileException.class)
    public void addCard_실패() {
        server.setMockField(new NxMileMockServer.MockField("1111", 38, 4, "실패"),
                            new NxMileMockServer.MockField("M481", 0, 4, "결과정상"));

        ifService.addCard("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678", "1234567890123456");
    }

    @Test
    public void addCombinedUseCard_정상() {
        server.setMockField(new NxMileMockServer.MockField("0000", 38, 4, "결과정상"),
                            new NxMileMockServer.MockField("A961", 0, 4, "결과정상"));

        ifService.addCombinedUseCard("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678", "1234567890123456");
    }

    @Test(expected = NxMileException.class)
    public void addCombinedUseCard_실패() {
        server.setMockField(new NxMileMockServer.MockField("1111", 38, 4, "실패"),
                            new NxMileMockServer.MockField("A961", 0, 4, "결과정상"));

        ifService.addCombinedUseCard("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678", "1234567890123456");
    }

    @Test
    public void searchOcbPoint_정상() {
        server.setMockField(new NxMileMockServer.MockField("0000", 38, 4, "결과정상"),
                            new NxMileMockServer.MockField("0000001000", 263, 10, "가용포인트"),
                            new NxMileMockServer.MockField("W401", 0, 4, "결과정상"));

        long usablePoint = ifService.searchOcbPoint("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678", "1234567890123456");
        Assert.assertEquals(1000L, usablePoint);
    }

    @Test
    public void useOcbPoint_정상() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK401 mock = new NxMileIFK401();
        mock.setMsgGrpCd("K401");
        mock.setResCdScls("00");
        mock.setResCdLcls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);
        mock.setMessage3("1000");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.useOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getTxOcbPoint());
        assertEquals(1000, pointResDto.getDiscountPoint());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void useOcbPoint_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK401 mock = new NxMileIFK401();
        mock.setMsgGrpCd("K401");
        mock.setResCdLcls("11");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.useOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("1199", pointResDto.getResultCode());
    }

    @Test
    public void useOcbPoint_재시도() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK401 mock = new NxMileIFK401();
        mock.setMsgGrpCd("K401");
        mock.setResCdLcls("99");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.useOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.NETWORK_ERROR, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }

    @Test
    public void cancelOcbPoint_성공() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK411 mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("00");
        mock.setResCdScls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));

        mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("77");
        mock.setResCdScls("46");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        pointResDto = ifService.cancelUseOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("7746", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void cancelOcbPoint_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK411 mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("99");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }

    @Test
    public void cancelOcbPointForNwError_성공() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK411 mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("00");
        mock.setResCdScls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));

        mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("77");
        mock.setResCdScls("46");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        pointResDto = ifService.cancelUseOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("7746", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void cancelOcbPointForNwError_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK411 mock = new NxMileIFK411();
        mock.setMsgGrpCd("K411");
        mock.setResCdLcls("99");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }

    @Test
    public void chargeOCBPoint_정상() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK101 mock = new NxMileIFK101();
        mock.setMsgGrpCd("K101");
        mock.setResCdScls("00");
        mock.setResCdLcls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.chargeOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(2000, pointResDto.getTxOcbPoint());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void chargeOCBPoint_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK101 mock = new NxMileIFK101();
        mock.setMsgGrpCd("K101");
        mock.setResCdScls("11");
        mock.setResCdLcls("11");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.chargeOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("1111", pointResDto.getResultCode());
    }

    @Test
    public void chargeOCBPoint_재시도() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK101 mock = new NxMileIFK101();
        mock.setMsgGrpCd("K101");
        mock.setResCdScls("99");
        mock.setResCdLcls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointUseReqDto pointReqDto = new OCBPointUseReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTotalAmt(10000L);
        pointReqDto.setTxOcbPoint(2000L);

        OCBPointUseResDto pointResDto = ifService.chargeOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.NETWORK_ERROR, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }

    @Test
    public void cancelChargeOcbPoint_성공() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK111 mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("00");
        mock.setResCdScls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelChargeOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));

        mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("77");
        mock.setResCdScls("46");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        pointResDto = ifService.cancelChargeOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("7746", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void cancelChargeOcbPoint_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK111 mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("99");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPoint(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }

    @Test
    public void cancelChargeOcbPointForNwError_성공() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK111 mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("00");
        mock.setResCdScls("00");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setCnt(2);
        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelChargeOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("0000", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));

        mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("77");
        mock.setResCdScls("46");
        mock.setApproveDate(DateTimeFormatter.ofPattern("yyyyMMdd").format(now));
        mock.setApproveTime(DateTimeFormatter.ofPattern("HHmmss").format(now));
        mock.setApproveNo("123456789");
        mock.setUsablePoint1(1000L);

        mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        pointResDto = ifService.cancelChargeOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.SUCCESS, pointResDto.getResultType());
        assertEquals("7746", pointResDto.getResultCode());
        assertEquals(1000, pointResDto.getBalance());
        assertEquals("123456789", pointResDto.getApprovalNo());
        assertEquals(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(now),
                DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pointResDto.getApprovalDate()));
    }

    @Test
    public void cancelChargeOcbPointForNwError_실패() {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFK111 mock = new NxMileIFK111();
        mock.setMsgGrpCd("K111");
        mock.setResCdLcls("99");
        mock.setResCdScls("99");

        NxmileMessageConverter converter = new NxmileMessageConverter("euc-kr");
        String mockValue = converter.encode(mock);

        server.setMockAllField(new NxMileMockServer.MockAllFieldReplace(mockValue, 0, mockValue.getBytes().length, "결과정상"));

        OCBPointCancelReqDto pointReqDto = new OCBPointCancelReqDto();
        pointReqDto.setTrackingNo(String.valueOf(new Random().nextInt(1000000000)));
        pointReqDto.setMctNo("90137233");
        pointReqDto.setBizNo(1048636968);
        pointReqDto.setTxDate(now);
        pointReqDto.setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");
        pointReqDto.setCrdNo("1234567890123456");
        pointReqDto.setTxOcbPoint(2000L);
        pointReqDto.setOrgTxDate(now.minusHours(1L));
        pointReqDto.setOrgApprovalNo("123456789");
        pointReqDto.setOrgTxOcbPoint(2000L);

        OCBPointCancelResDto pointResDto = ifService.cancelUseOcbPointForNwError(pointReqDto);

        assertNotNull(pointResDto);
        assertEquals(TxResType.FAIL, pointResDto.getResultType());
        assertEquals("9999", pointResDto.getResultCode());
    }
}