package com.skplanet.okx.pg.api.mapper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.api.model.OCBMapDto;
import com.skplanet.okx.pg.constant.PGConstants;


@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class OCBMapperTest {

	@Autowired
	OCBMapper ocbMap;

	private static String crdNo = "XZ00999999999999";
	private static long userSn = 90000001;
	
	private static String crdNo2 = "XZ00999999999998";
	private static long userSn2 = 90000002;
	
	
	@Before
	public void setUp() throws Exception {
		insertHelper(crdNo2, userSn2, PGConstants.PG_OKX_STATUS_ON);
	}

	private int insertHelper(String crdNo, long userSn, String status) {
		OCBMapDto ocbObj = new OCBMapDto();
		ocbObj.setCrdNo(crdNo);
		ocbObj.setUserSn(userSn);
		ocbObj.setOcbStatus(status);
		
		return ocbMap.insertUpdateOCBMap(ocbObj);
	}
	
	@Test
	public void insertUpdate_입력() {		
		int rows = insertHelper(crdNo, userSn, PGConstants.PG_OKX_STATUS_ON);
		
		assertEquals(1, rows);
	}
	
	@Test
	public void insertUpdate_수정() {

		int rows = insertHelper(crdNo, userSn, PGConstants.PG_OKX_STATUS_ON);
		rows = insertHelper(crdNo, userSn, PGConstants.PG_OKX_STATUS_OFF);
		
		assertEquals(1, rows);
	}

	@Test
	public void findOcbMapByUserSn_실패() {
		OCBMapDto result = ocbMap.findOcbMapByUserSn(userSn);
		assertNull(result);
		
	}	

	@Test
	public void findOcbMapByUserSn_성공() {
		OCBMapDto result = ocbMap.findOcbMapByUserSn(userSn2);
		assertNotNull(result);
		
	}	

	@Test
	public void findOcbMapByCrdNo_성공() {
		OCBMapDto result = ocbMap.findOcbMapByCrdNo(crdNo2);
		assertNotNull(result);
		
	}	
	
	
}
