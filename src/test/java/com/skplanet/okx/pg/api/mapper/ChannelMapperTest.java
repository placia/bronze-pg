package com.skplanet.okx.pg.api.mapper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.constant.PGConstants;


@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ChannelMapperTest {

	@Autowired
	ChannelMapper chMap;
	
	private static String CH_ID = "test";
	private static String CH_PW = "_testkey_";
	
	
	@Before
	public void setUp() throws Exception {
		chMap.insertChannel(CH_ID, CH_PW, PGConstants.PG_OKX_STATUS_ON);
	}
	
	@Test
	public void testGetChStatus() {
		String result = chMap.getChStatus(CH_ID, CH_PW);
		assertEquals(PGConstants.PG_OKX_STATUS_ON, result);
	}

}
