package com.skplanet.okx.pg.api.service;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.api.dto.bif.CreateWalletRes;
import com.skplanet.okx.pg.api.dto.bif.PaymentInfo;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class BIFServiceTest {

	@Autowired
	private BIFService bif;
	private long userSn = 9000000001L;
	private long targetUserSn = 9000000002L;
	private long price = 10000L;
	

    @Before
    public void setUp() {

    }
    
	@Test
	public void createWallet_성공() {
		CreateWalletRes result = bif.createWallet(userSn );
		assertNotNull(result);
	}

	@Test
	public void getPaymentInfo_성공() {
		PaymentInfo result = bif.getPaymentInfo(userSn, targetUserSn, price);
		assertNotNull(result);
	}

	@Test
	public void executePayment_성공() {
		//result = bif.executePayment(userSn, targetUserSn, totalPrice, clientTx, clientJson, tokenAmountMap)
	}

}
