package com.skplanet.okx.pg.api.service;

import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFA960;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFM480;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFW400;
import com.skplanet.okx.pg.support.exception.impl.NxMileException;
import org.junit.Before;
import org.junit.Test;

public class NxMileFailHandleServiceTest {
    NxMileFailHandleService predicatesService;

    @Before
    public void setUp() {
        predicatesService = new NxMileFailHandleService();
    }

    @Test
    public void W400_성공() {
        NxMileIFW400 req = NxMileIFW400.of()
                .setCi("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678")
                .build()
        ;

        NxMileIFW400 resp = NxMileIFW400.of()
                .setResCdLcls("00")
                .setResCdScls("00")
                .build()
                ;

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void W400_탈회상태() {
        NxMileIFW400 req = NxMileIFW400.of()
                .setCi("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678")
                .build()
        ;

        NxMileIFW400 resp = NxMileIFW400.of()
                .setIsAccumableType("N")
                .setIsUsableType("Y")
                .setResCdLcls("00")
                .setResCdScls("00")
                .setMsgGrpCd("W401")
                .build();
        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void W400_회원상태비정상() {
        NxMileIFW400 req = NxMileIFW400.of()
                .setCi("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678")
                .build()
        ;

        NxMileIFW400 resp = NxMileIFW400.of()
                .setIsAccumableType("N")
                .setIsUsableType("Y")
                .setResCdLcls("44")
                .setResCdScls("29")
                .setMsgGrpCd("W401")
                .build();

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void W400_그외오류() {
        NxMileIFW400 req = NxMileIFW400.of()
                .setCi("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678")
                .build()
        ;

        NxMileIFW400 resp = NxMileIFW400.of()
                .setResCdLcls("41")
                .setResCdScls("29")
                .setMsgGrpCd("W401")
                .build();

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void M480_성공() {
        NxMileIFM480 req = NxMileIFM480.of()
                .setCardNo("1234567890123456")
                .build()
        ;

        NxMileIFM480 resp = NxMileIFM480.of()
                .setResCdLcls("00")
                .setResCdScls("00")
                .build()
                ;

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void M480_실패() {
        NxMileIFM480 req = NxMileIFM480.of()
                .setCardNo("1234567890123456")
                .build()
        ;

        NxMileIFM480 resp = NxMileIFM480.of()
                .setResCdLcls("10")
                .setResCdScls("00")
                .setMsgGrpCd("M481")
                .build();

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void A960_성공() {
        NxMileIFA960 req = NxMileIFA960.of()
                .setCardNo("1234567890123456")
                .build()
        ;

        NxMileIFA960 resp = NxMileIFA960.of()
                .setResCdLcls("00")
                .setResCdScls("00")
                .build()
                ;

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test(expected = NxMileException.class)
    public void A960_실패() {
        NxMileIFA960 req = NxMileIFA960.of()
                .setCardNo("1234567890123456")
                .build()
                ;

        NxMileIFA960 resp = NxMileIFA960.of()
                .setResCdLcls("10")
                .setResCdScls("00")
                .setMsgGrpCd("A961")
                .build();

        try {
            predicatesService.test(req, resp);
        } catch (NxMileException e) {
            e.printStackTrace();
            throw e;
        }
    }
}