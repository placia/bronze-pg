package com.skplanet.okx.pg.api.controller;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.api.dto.nxmile.*;
import com.skplanet.okx.pg.api.service.external.nxmile.NxMileConn;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
//@AutoConfigureMockMvc
public class NxMileControllerTest {
    @Autowired
    private NxMileConn nxMileConn;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void NXMILE_합산사용요청() throws IOException {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFDD10 request = NxMileIFDD10.of()
                .setMerchantNo("953871269")
                .setTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setTrxTime(now.format(DateTimeFormatter.ofPattern("HHmmss")))
                .setTrackData("KX00000000000001")
                .setPartnerMerchantName("훼미리마트 성북점")
                .setPartnerMerchantNo("98022684")
                .setPartnerApproveNo("521000001")
                .setSlipCd("11")
                .setTrxTypeCd("40")
                .setComplexPayYN("N")
                .setTrxAmtTotSum(50000L)
                .setContractTypeCd1("08")
                .setTrxAmt1(30000L)
                ;

        NxMileIFDD11 response = nxMileConn.sendAndReceive(request, NxMileIFDD11.class);

        assertNotNull(response);
        assertEquals("DD11", response.getMsgGrpCd());
        assertEquals(request.getTrackingNo(), response.getTrackingNo());
        assertEquals(NxMileResultCode.SUCCESS.value(), response.getResCdLcls()+response.getResCdScls());
        assertNotNull(response.getApproveNo());
    }

    @Test
    public void NXMILE_취소요청() throws IOException {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFDD20 request = NxMileIFDD20.of()
                .setMerchantNo("953871269")
                .setTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setTrxTime(now.format(DateTimeFormatter.ofPattern("HHmmss")))
                .setTrackData("KX00000000000001")
                .setPartnerMerchantName("훼미리마트 성북점")
                .setPartnerMerchantNo("98022684")
                .setPartnerApproveNo("521000002")       //OCB 취소승인번호
                .setOrgTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd"))) //OCB원거래일자
                .setOrgApproveNo("000000001")           //승인응답 거래번호
                .setOrgTrxPartnerApproveNo("521000001") //OCB 원거래승인번호
                .setSlipCd("22")
                .setTrxTypeCd("42")
                .setComplexPayYN("N")
                .setOrgTrxAmt(3000L)
                ;

        NxMileIFDD11 response = nxMileConn.sendAndReceive(request, NxMileIFDD11.class);

        assertNotNull(response);
        assertEquals("DD21", response.getMsgGrpCd());
        assertEquals(request.getTrackingNo(), response.getTrackingNo());
        assertEquals(NxMileResultCode.SUCCESS.value(), response.getResCdLcls()+response.getResCdScls());
        assertNotNull(response.getApproveNo());
    }

    @Test
    public void NXMILE_망취소요청() throws IOException {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFDD20 request = (NxMileIFDD20) NxMileIFDD20.of()
                .setMerchantNo("953871269")
                .setTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setTrxTime(now.format(DateTimeFormatter.ofPattern("HHmmss")))
                .setTrackData("KX00000000000001")
                .setPartnerMerchantName("훼미리마트 성북점")
                .setPartnerMerchantNo("98022684")
                .setPartnerApproveNo("521000003")       //OCB 사용 승인번호
                .setOrgTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setSlipCd("22")
                .setTrxTypeCd("42")
                .setComplexPayYN("N")
                .setOrgTrxAmt(3000L)
                .setResCdLcls("60");

        NxMileIFDD11 response = nxMileConn.sendAndReceive(request, NxMileIFDD11.class);

        assertNotNull(response);
        assertEquals("DD21", response.getMsgGrpCd());
        assertEquals(request.getTrackingNo(), response.getTrackingNo());
        assertEquals(NxMileResultCode.SUCCESS.value(), response.getResCdLcls()+response.getResCdScls());
        assertNotNull(response.getApproveNo());
    }


    @Test
    public void NXMILE_잔액조회요청() throws IOException {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFDD30 request = NxMileIFDD30.of()
                .setMerchantNo("953871269")
                .setTrackData("KX00000000000001")
                .setRequestType("RD");

        NxMileIFDD31 response = nxMileConn.sendAndReceive(request, NxMileIFDD31.class);

        assertNotNull(response);
        assertEquals("DD31", response.getMsgGrpCd());
        assertEquals("Y", response.getIsUsableType());
        assertTrue(response.getUsablePoint1() > 0);
    }

    @Test
    public void NXMIL_사용망상재사용() throws IOException {
        LocalDateTime now = LocalDateTime.now();

        NxMileIFDD40 request = NxMileIFDD40.of()
                .setMerchantNo("953871269")
                .setTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .setTrxTime(now.format(DateTimeFormatter.ofPattern("HHmmss")))
                .setTrackData("KX00000000000001")
                .setPartnerMerchantName("훼미리마트 성북점")
                .setPartnerMerchantNo("98022684")
                .setPartnerApproveNo("521000004")       //OCB 사용취소 승인번호
                .setOrgTrxDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd"))) //OCB원거래일자
                .setOrgApproveNo("000000001")           //승인응답 거래번호
                .setOrgTrxPartnerApproveNo("521000001") //OCB 원거래승인번호
                .setSlipCd("11")
                .setTrxTypeCd("40")
                .setComplexPayYN("N")
                .setOrgTrxAmt(3000L)
                ;

        NxMileIFDD41 response = nxMileConn.sendAndReceive(request, NxMileIFDD41.class);

        assertNotNull(response);
        assertEquals("DD41", response.getMsgGrpCd());
        assertEquals(request.getTrackingNo(), response.getTrackingNo());
        assertEquals(NxMileResultCode.SUCCESS.value(), response.getResCdLcls()+response.getResCdScls());
        assertNotNull(response.getApproveNo());
    }
}