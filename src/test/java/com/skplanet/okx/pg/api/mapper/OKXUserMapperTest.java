package com.skplanet.okx.pg.api.mapper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.api.model.OKXUser;
import com.skplanet.okx.pg.constant.PGConstants;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class OKXUserMapperTest {

	@Autowired
	OKXUserMapper userMap;

	long userSn = 900000001;
	String userKey = "_user_key_for_test_";
	String walletKey = "_wallet_key_for_test_";
	String userStatus = PGConstants.PG_OKX_STATUS_ON;
	

	long userSn2 = 900000002;
	String userKey2 = "_user_key_for_test2_";
	String walletKey2 = "_wallet_key_for_test2_";
	String userStatus2 = PGConstants.PG_OKX_STATUS_ON;
	
	
	@Before
	public void setUp() throws Exception {
		int result = insertHelper(userSn2, userKey2, walletKey2, userStatus2);
	}

	@Test
	public void insertUser_성공() {

		
		int result = insertHelper(userSn, userKey, walletKey, userStatus);
		
		assertEquals(1, result);
	}

	private int insertHelper(long userSn, String userKey, String walletKey, String status) {
		OKXUser user = new OKXUser();
		user.setUserSn(userSn);
		user.setUserKey(userKey);
		user.setWalletKey(walletKey);
		user.setUserStatus(status);
		
		int result = userMap.insertUser(user);
		return result;
	}
	
	@Test
	public void findUserByUserSn_성공() {

		OKXUser result = userMap.findUserByUserSn(userSn2);
		assertNotNull(result);
	}
	
	@Test
	public void findUserByUserKey_성공() {

		OKXUser result = userMap.findUserByUserKey(userKey2);
		assertNotNull(result);
	}
	
	
	
	
	

}
