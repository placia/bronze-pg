package com.skplanet.okx.pg.api.service.mock;

import lombok.RequiredArgsConstructor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;

public class NxMileMockServer {
    MockField[] mockFields;
    MockAllFieldReplace mockAllField;
    private int port;
    private int cnt = 1;

    public void start() {
        try (ServerSocket server = new ServerSocket(port);
        ) {
            for (int i = 0; i < cnt; i++) {
                try (
                        Socket sock = server.accept();
                        BufferedInputStream bis = new BufferedInputStream(sock.getInputStream());
                        BufferedOutputStream bos = new BufferedOutputStream(sock.getOutputStream())
                ) {
                    byte[] header = new byte[50];
                    bis.read(header, 0, header.length);

                    byte[] body = new byte[Integer.parseInt(new String(header, 34, 4, "euc-kr"))];
                    bis.read(body, 0, body.length);

                    byte[] req = new byte[header.length + body.length];
                    System.arraycopy(header, 0, req, 0, header.length);
                    System.arraycopy(body, 0, req, header.length, body.length);

                    System.out.println(String.format("[SERVER RECV][%d][%s]", req.length, new String(req, "euc-kr")));

                    String msgGrpCd = new String(req, 0, 4, "euc-kr");

                    if (Objects.isNull(mockAllField)) {
                        for (MockField mockField : mockFields) {
                            System.arraycopy(mockField.value.getBytes(), 0, req, mockField.offset, mockField.length);
                        }
                    } else {
                        req = new byte[mockAllField.length];
                        System.arraycopy(mockAllField.value.getBytes(), 0, req, 0, mockAllField.length);
                    }

                    System.out.println(String.format("[SERVER SEND][%d][%s]", req.length, new String(req, "euc-kr")));
                    bos.write(req, 0, req.length);
                    bos.flush();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void setMockField(MockField ... mockFields) {
        this.mockFields = mockFields;
    }

    public void setMockAllField(MockAllFieldReplace mockAllField) {
        this.mockAllField = mockAllField;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    @RequiredArgsConstructor
    public static class MockField {
        public final String value;
        public final int offset;
        public final int length;
        public final String desc;
    }

    @RequiredArgsConstructor
    public static class MockAllFieldReplace {
        public final String value;
        public final int offset;
        public final int length;
        public final String desc;
    }

    public static void main(String[] args) {
        NxMileMockServer server = new NxMileMockServer();
        server.setPort(10001);
        server.setMockField(new NxMileMockServer.MockField("9999", 38, 4, "결과정상"));
        server.start();
    }
}
