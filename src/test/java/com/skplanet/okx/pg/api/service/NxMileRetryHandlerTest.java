package com.skplanet.okx.pg.api.service;

import com.skplanet.ocb.tcp.netty.TcpClient;
import com.skplanet.okx.pg.api.dto.nxmile.NxMileIFW400;
import com.skplanet.okx.pg.api.service.external.nxmile.NxMileRetryHandler;
import com.skplanet.okx.pg.api.service.mock.NxMileMockServer;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class NxMileRetryHandlerTest {
    TcpClient tcpClient;
    static ExecutorService executor;
    static NxMileMockServer server = null;

    @Before
    public void setUp() throws Exception {
        tcpClient = TcpClient.of("localhost", 10001, "euc-kr");

        executor = Executors.newFixedThreadPool(10);
        server = new NxMileMockServer();
        server.setPort(10001);
        server.setCnt(2);

        executor.submit(() -> {
            server.start();
        });

        TimeUnit.MILLISECONDS.sleep(500);
    }

    @Test
    public void perform() {
        NxMileIFW400 request = NxMileIFW400.of()
                .setCi("0987654321098765432109876543210987654321098765432109876543210987654321098765432112345678");

        server.setMockField(new NxMileMockServer.MockField("9999", 38, 4, "결과정상"));

        NxMileRetryHandler<NxMileIFW400, NxMileIFW400> retryHandler =
                new NxMileRetryHandler<>(tcpClient, 2, resp -> "9999".equals(resp.getResultCode()));

        try {
            Optional<NxMileIFW400> resp = retryHandler.perform(request, NxMileIFW400.class, req -> req.setResCdLcls("60"));
            resp.ifPresent(x -> log.info(x.getResultCode()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}