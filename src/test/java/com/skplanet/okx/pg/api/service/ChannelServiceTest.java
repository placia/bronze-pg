package com.skplanet.okx.pg.api.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.okx.pg.Application;
import com.skplanet.okx.pg.api.dto.user.verifier.Verifier;
import com.skplanet.okx.pg.api.mapper.ChannelMapper;
import com.skplanet.okx.pg.api.mapper.OKXUserMapper;
import com.skplanet.okx.pg.api.model.OKXUser;
import com.skplanet.okx.pg.constant.PGConstants;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ChannelServiceTest {

	@Autowired
	ChannelService channelService;
	
	@Autowired
	ChannelMapper channelMapper;

	@Autowired
	OKXUserMapper userMapper;
	
	private String chId = "testChId";
	private String chKey = "_testChKey_";
	private String chStatus = PGConstants.PG_OKX_STATUS_ON;
	
	private long userSn = 900000001;
	String userKey = "_user_key_for_test_";
	String walletKey = "_wallet_key_for_test_";
	

	
	@Before
	public void setUp() throws Exception {
		OKXUser user = new OKXUser();
		user.setUserSn(userSn);
		user.setWalletKey(walletKey);
		user.setUserKey(userKey);
		user.setUserStatus(chStatus);
		userMapper.insertUser(user );
		channelMapper.insertChannel(chId, chKey, chStatus);
	}

	@Test
	public void genVerifier_성공() {
		
		String result = channelService.genVerifier(chId, chKey, userSn);
		assertNotNull(result);
		
	}

	@Test
	public void decodeVerifier_성공() {

		String vkey = channelService.genVerifier(chId, chKey, userSn);
		
		Verifier result = channelService.decodeVerifier(vkey);
		assertNotNull(result);
		
	}
	
	@Test
	public void validateChannel_성공() {
		
		channelService.validateChannel(chId, chKey);
		
	}
	
}
