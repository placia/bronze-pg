#!/bin/bash
SERVICE_BASE_PATH=/app
APP_PATH=$SERVICE_BASE_PATH/deploy/pg
APP_CLASSPATH=$APP_PATH/lib
MAIN_CLASS_NAME=com.skplanet.okx.pg.Application
PIDFile=".app.pid"

cd $APP_PATH
        
startup() {
        for filename in $APP_CLASSPATH/*.jar
        do
                CLASSPATH=$filename
        done

        JAVA_OPT=" -server -Xms512m -Xmx2048m -Djava.security.egd=file:/dev/./urandom -Dfile.encoding=UTF-8 -Dspring.profiles.active=dev "
        
        JAVA_HOME=$SERVICE_BASE_PATH/jdk
#       echo $JAVA_OPT
#       echo $CLASSPATH
        nohup $JAVA_HOME/bin/java $JAVA_OPT -jar $CLASSPATH 1> $APP_PATH/logs/app.log 2>&1 &
        echo $! > $PIDFile

}


check_if_pid_file_exists() {
    if [ ! -f $PIDFile ]
    then
 echo "PID file not found: $PIDFile"
        exit 1
    fi
} 
 
check_if_process_is_running() {
 if ps -p $(print_process) > /dev/null
 then
     return 0
 else
     return 1
 fi
}
 
print_process() {
    echo $(<"$PIDFile")
}
 
case "$1" in
  status)
    check_if_pid_file_exists
    if check_if_process_is_running
    then
      echo $(print_process)" is running"
    else
      echo "Process not running: $(print_process)"
    fi
    ;;
  stop)
    check_if_pid_file_exists
    if ! check_if_process_is_running
    then
      echo "Process $(print_process) already stopped"
      exit 0
    fi
    kill -TERM $(print_process)
    echo -ne "Waiting for process to stop"
    NOT_KILLED=1
    for i in {1..20}; do
      if check_if_process_is_running
      then
        echo -ne "."
        sleep 1
      else
        NOT_KILLED=0
      fi
    done
    echo
    if [ $NOT_KILLED = 1 ]
    then
      echo "Cannot kill process $(print_process)"
      exit 1
    fi
    echo "Process stopped"
    ;;
  start)
    if [ -f $PIDFile ] && check_if_process_is_running
    then
      echo "Process $(print_process) already running"
      exit 1
    fi
    startup
    echo "Process started"
    ;;
  restart)
    $0 stop
    if [ $? = 1 ]
    then
      exit 1
    fi
    $0 start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|status}"
    exit 1
esac
 
exit 0